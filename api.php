<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define('AJAX_SCRIPT', true);
require_once('../../config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');
$err = new stdClass();
$context = context_system::instance();
$PAGE->set_context($context);
require_login(null, false);

if (isguestuser()) {
    print_error('noguest');
}

if (!confirm_sesskey()) {
    $err->error = get_string('invalidsesskey', 'error');
    die(json_encode($err));
}
$action = optional_param('action', '', PARAM_ALPHA);
$term = optional_param('term', '', PARAM_ALPHA);

$data = new stdclass;

switch ($action) {
    case 'registerrole':
        $request_body = file_get_contents('php://input');
        $json = !empty($request_body) ? json_decode($request_body) : null;
        if (json_last_error() != JSON_ERROR_NONE || empty($json)) {
            throw new Exception('Invalid reqest body');
        }

        $data = $json;
        if (!empty($json->roles)) {
            try {
                $data = umass_api_process_user_request($json->roles);
                //NEINDEX
                update_custom_fields();
                //update_custom_fields_from_roles();
            } catch (\Exception $ex) {
                throw $ex;
            }
        }
        echo json_encode($data);
        break;
    case 'search':
        $agencytype = required_param('agencytype', PARAM_ALPHA);
        if (!in_array($agencytype, array('odp', 'ae'))) {
            throw new Exception('Invalid agency type');
        }

        $rs = $DB->get_records_select('local_umass_agency_' . $agencytype, "entity LIKE ?", array('%'.strtolower($term).'%'));
        $results = array();
        foreach ($rs as $record) {
            $results[$record->id] = $record;
        }
        echo json_encode($results);
        break;
    case 'setagency':
        $agencytype = required_param('agencytype', PARAM_ALPHA);
        if (!in_array($agencytype, array('ae', 'odp', 'writein'))) {
            throw new Exception('Invalid agency type');
        }

        $agencyid = required_param('agencyid', PARAM_NOTAGS);
        if (empty($agencyid)) {
            throw new Exception('No agency selected.');
        }

        if ($userdata = $DB->get_record('local_umass_user_data', array('userid' => $USER->id))) {
            $userdata->agencytype = $agencytype;
            switch ($agencytype) {
                case 'odp':
                    $agencyid = (int)$agencyid;
                    $userdata->odp_agencyid = $agencyid;
                    $userdata->ae_agencyid = 0;
                    break;
                case 'ae':
                    $agencyid = (int)$agencyid;
                    $userdata->ae_agencyid = $agencyid;
                    $userdata->odp_agencyid = 0;
                    break;
                case 'writein':
                    $userdata->writein_agencyid = clean_param($agencyid, PARAM_NOTAGS);
                    $userdata->ae_agencyid = 0;
                    $userdata->odp_agencyid = 0;
                    break;
            }
            $DB->update_record('local_umass_user_data', $userdata);

            $userdata = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));
            $userdata->ok = true;
            $userdata->data = umass_api_process_umassroles_for_user();
            list($message, $notice, $roles) = umass_api_agency_page_message_builder();
            $userdata->message = $message;
            $userdata->notice = $notice;
            $userdata->roles = $roles;
        } else {
            $userdata = array('error' => 'something bad happened');
        }

        echo json_encode($userdata);
        break;
}
