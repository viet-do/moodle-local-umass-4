<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(__DIR__ . '/locallib.php');
require_once(__DIR__ . '/lib.php');

function umass_is_register_processed() {
    $result = true;
    global $DB, $PAGE, $CFG, $DB, $USER;
    $path = $PAGE->url->out_as_local_url();
    if (preg_match('#local\/umass#', $path)) {
        // always allow local/umass path
        return true;
    }
    $exceptionroles = array(
        'ppa',
        'ppa_aaw',
        'ppa_pfdsw',
        'others_odp',
    );
    $sql = 'SELECT r.*
              FROM {local_umass_role} r
              JOIN {local_umass_user_role} ur ON ur.umassroleid=r.id
             WHERE ur.userid=?';
    $roles = $DB->get_records_sql($sql, array($USER->id));
    $ppaorodp = false;
    foreach ($roles as $role) {
        if (in_array($role->shortname, $exceptionroles)) {
            $ppaorodp = true;
        }
    }
    if ($record = $DB->get_record('local_umass_user_data', array('userid' => $USER->id))) {
        $metarole = $record->metarole;
        if (empty($metarole)) {
            return false;
        }
        if ($metarole & ROLE_PROFESSIONAL) {
            $agencyset = (!empty($record->ae_agencyid || !empty($record->odp_agencyid)
                || !empty($record->writein_agencyid)));

            // agency being set or user is odp/ppa, agency isn't needed
            if ($ppaorodp || (!empty($record->agencytype) && $agencyset)) {
                $result = true;
            } else {
                $result = false;
            }
        }
        if ($metarole & ROLE_FICA) {
            if (empty($record->county)) {
                $result = false;
            }
        }
        if ($metarole & ROLE_OMHSAS) {
            return false;
        }
    } else {
        return false;
    }
    return $result;
}

$desturl = new moodle_url('/local/umass/register.php');
// guest user don't have to redirect
if ((isloggedin() && !umass_is_register_processed()) && !isguestuser()) {
    redirect(new moodle_url($desturl));
}
