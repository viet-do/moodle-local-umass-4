<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class groupform extends \moodleform
{
    protected function definition() {
        $mform = $this->_form;

        $mform->addElement('select', 'group', get_string('group', 'local_umass'), $this->groups());
        $mform->addRule('group', get_string('required'), 'required', null, 'client');
        $mform->setType('group', PARAM_NOTAGS);
        $mform->addHelpButton('group', 'group', 'local_umass');

        $mform->addElement(
            'select',
            'umassroleid',
            get_string('role', 'local_umass'),
            $this->umass_roles()
        );
        $mform->addRule('umassroleid', get_string('required'), 'required', null, 'client');
        $mform->setType('umassroleid', PARAM_INT);
        $mform->addHelpButton('umassroleid', 'role', 'local_umass');

        $this->add_action_buttons();
    }

    private function groups() {
        global $UMASS_GROUPS;
        return $UMASS_GROUPS;
    }

    private function umass_roles() {
        global $DB;
        $records = $DB->get_records('local_umass_role');
        $roles = array();
        foreach ($records as $record) {
            $roles[$record->id] = $record->fullname;
        }

        return $roles;
    }
}
