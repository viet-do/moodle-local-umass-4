<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class cohortform extends \moodleform
{
    protected function definition() {
        $mform = $this->_form;

        $mform->addElement('select', 'cohortid', get_string('cohort', 'local_umass'), $this->cohorts());
        $mform->addRule('cohortid', get_string('required'), 'required', null, 'client');
        $mform->setType('cohortid', PARAM_INT);
        $mform->addHelpButton('cohortid', 'cohort', 'local_umass');

        $mform->addElement(
            'select',
            'umassroleid',
            get_string('role', 'local_umass'),
            $this->umass_roles()
        );
        $mform->addRule('umassroleid', get_string('required'), 'required', null, 'client');
        $mform->setType('umassroleid', PARAM_INT);
        $mform->addHelpButton('umassroleid', 'role', 'local_umass');

        $this->add_action_buttons();
    }

    private function cohorts() {
        // $cohorts = cohort_get_cohorts(\context_system::instance()->id, null, null);
        $cohorts = cohort_get_all_cohorts(0, 100);

        $result = array();
        foreach ($cohorts['cohorts'] as $cohort) {
            $result[$cohort->id] = $cohort->name;
        }

        return $result;
    }

    private function umass_roles() {
        global $DB;
        $records = $DB->get_records('local_umass_role');
        $roles = array();
        foreach ($records as $record) {
            $roles[$record->id] = $record->fullname;
        }

        return $roles;
    }
}
