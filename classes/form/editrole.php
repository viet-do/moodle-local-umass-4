<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class editrole extends \moodleform
{
    public function definition() {
        global $CFG;
        $mform = $this->_form;

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $attributes = array(
            'placeholder' => 'Role name...',
            'size' => 48,
        );
        $mform->addElement(
            'text',
            'fullname',
            get_string('fullname'),
            $attributes
        );
        $mform->setType('fullname', PARAM_NOTAGS);

        $mform->addElement(
            'select',
            'mailvalidation',
            get_string('mailvalidation', 'local_umass'),
            array('1' => get_string('yes'), '0' => get_string('no'))
        );
        $mform->setType('mailvalidation', PARAM_INT);

        $attributes['placeholder'] = 'Enter domain name each line...';
        $attributes['rows'] = 10;
        $attributes['cols'] = 48;
        $mform->addElement(
            'textarea',
            'domains',
            get_string('domains', 'local_umass'),
            $attributes
        );
        $mform->setType('domains', PARAM_NOTAGS);
        $mform->addHelpButton('domains', 'domains', 'local_umass');

        $this->add_action_buttons(false);
    }

    public function set_data($data) {
        parent::set_data($data);
    }

    public function validation($data, $files) {
        return array();
    }
}
