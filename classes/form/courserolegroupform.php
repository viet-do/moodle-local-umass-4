<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class courserolegroupform extends \moodleform
{
    private $courseid;
    // function moodleform($action=null, $customdata=null, $method='post', $target='', $attributes=null, $editable=true) {
    public function __construct($courseid, $action) {
        $this->courseid = $courseid;
        parent::__construct($action);
    }
    protected function definition() {
        $mform = $this->_form;

        $mform->addElement(
            'select',
            'umassroleid',
            get_string('role', 'local_umass'),
            $this->umass_roles()
        );
        $mform->addRule('umassroleid', get_string('required'), 'required', null, 'client');
        $mform->setType('umassroleid', PARAM_INT);
        $mform->addHelpButton('umassroleid', 'role', 'local_umass');

        $mform->addElement('select', 'groupid', get_string('group'), $this->course_groups());
        $mform->addRule('groupid', get_string('required'), 'required', null, 'client');
        $mform->setType('groupid', PARAM_INT);
        $mform->addHelpButton('groupid', 'cohort', 'local_umass');

        $mform->addElement('select', 'roleid', get_string('role'), $this->course_roles());
        $mform->addRule('roleid', get_string('required'), 'required', null, 'client');
        $mform->setType('roleid', PARAM_INT);
        $mform->addHelpButton('roleid', 'courserole', 'local_umass');

        $this->add_action_buttons();
    }
    private function course_groups() {
        global $CFG;
        $results = array();
        require_once($CFG->dirroot . '/group/lib.php');
        $groups = groups_get_all_groups($this->courseid);
        $results[0] = '-';
        foreach ($groups as $group) {
            $results[$group->id] = $group->name;
        }
        return $results;
    }

    private function course_roles() {
        global $DB;
        $rs = $DB->get_recordset('role');
        $result = array();
        $result[0] = '-';
        foreach ($rs as $role) {
            $result[$role->id] = $role->name ? $role->name : $role->shortname;
        }
        return $result;
    }

    private function umass_roles() {
        global $DB;
        $records = $DB->get_records('local_umass_role');
        $roles = array();
        foreach ($records as $record) {
            $roles[$record->id] = $record->fullname;
        }

        return $roles;
    }
}
