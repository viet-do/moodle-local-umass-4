<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class editae extends \moodleform
{
    public function definition() {
        global $CFG, $UMASS_COUNTIES;
        $mform = $this->_form;

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $attributes = array(
        );
        $mform->addElement(
            'text',
            'entity',
            'Entity',
            $attributes
        );
        $mform->setType('entity', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'county',
            'County'
        );
        $mform->setType('county', PARAM_ALPHA);

        $mform->addElement(
            'text',
            'address1',
            'Address 1'
        );
        $mform->setType('address1', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'address2',
            'Address 2'
        );
        $mform->setType('address2', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'city',
            'City'
        );
        $mform->setType('city', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'state',
            'State'
        );
        $mform->setType('state', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'zip',
            'ZIP'
        );
        $mform->setType('zip', PARAM_NOTAGS);

        $mform->addElement(
            'text',
            'phone',
            'Phone'
        );
        $mform->setType('phone', PARAM_NOTAGS);
        $mform->addElement(
            'text',
            'email',
            'Email'
        );
        $mform->setType('email', PARAM_EMAIL);
        $this->add_action_buttons();
    }

    public function set_data($data) {
        parent::set_data($data);
    }

    public function validation($data, $files) {
        return array();
    }
}
