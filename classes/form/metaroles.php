<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class metaroles extends \moodleform
{
    public function definition() {
        $mform = $this->_form;
        $attributes = array();
        $radioarray = array();
        $radioarray[] = $mform->createElement(
            'radio',
            'metarole',
            null,
            get_string('local_fica', 'local_umass'),
            ROLE_FICA,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'metarole',
            null,
            get_string('local_professional', 'local_umass'),
            ROLE_PROFESSIONAL,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'metarole',
            null,
            get_string('local_ficaprofessional', 'local_umass'),
            ROLE_PROFESSIONALANDFICA,
            $attributes
        );
   
        $bcontent = '<p><strong>Select the option that most appropriately describes your role related to Office of Developmental Programs OR Office of Mental Health and Substance Abuse Services</strong/></p>';
        $mform->addElement('html', $bcontent);

        $hrcontent = '<hr>';
        //$mform->addElement('html', $hrcontent);

        $mform->addElement('header', 'nameforyourheaderelement', 'For Office of Developmental Programs Learners');

        $separator = '<br><br>';
        $mform->addGroup($radioarray, 'rolecategory', '', $separator, false);

        $radio_omh[] = $mform->createElement(
            'radio',
            'metarole',
            null,
            get_string('local_omhsas', 'local_umass'),
            ROLE_OMHSAS,
            $attributes
        );
        
        //$mform->addElement('html', $hrcontent);

        $mform->addElement('header', 'nameforyourheaderelement', 'For Office of Mental Health and Substance Abuse Services Learners');

        $mform->addGroup($radio_omh, 'rolecategory', '', $separator, false);
        //$mform->addElement('html', $hrcontent);

        $this->add_action_buttons(false, get_string('next'));
    }

    public function validation($data, $files) {
        return array();
    }
}
