<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class omhexists extends \moodleform
{
    protected function definition() {
        $mform = $this->_form;
        //County where you live 
        $attributes = array();
        //$mform->addElement(
        //    'select',
        //    'county',
        //    get_string('county', 'local_umass'),
        //    local_umass_get_counties(),
        //    $attributes
        //);
        // Are you currently a member of the statewide Mental Health Planning Council (MHPC)?
        //$radioarray=array();
        //$radioarray[] = $mform->createElement('radio', 'membermhpc', '', get_string('yes'), 1, $attributes);
        //$radioarray[] = $mform->createElement('radio', 'membermhpc', '', get_string('no'), 0, $attributes);
        //$mform->addGroup($radioarray, 'membergroup', 'Are you currently a member of the statewide Mental Health Planning Council (MHPC)?', array(' '), false);
        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('cancel', 'cancelbutton', get_string('previous'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('next'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
    public function validation($data, $files) {
        global $DB, $UMASS_COUNTIES;
        return array();
    }
}
