<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_umass\form;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

class omhform extends \moodleform
{
    protected function definition() {
        $mform = $this->_form;
        $attributes = array();
        $radioarray = array();
        $radioarray[] = $mform->createElement(
            'radio',
            'omhsasrole',
            null,
            get_string('omh_role_ind_nonpro', 'local_umass'),
            ROLE_OMH_IND_NONPRO,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'omhsasrole',
            null,
            get_string('omh_role_ind_pro', 'local_umass'),
            ROLE_OMH_IND_PRO,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'omhsasrole',
            null,
            get_string('omh_role_pro', 'local_umass'),
            ROLE_OMH_PRO,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'omhsasrole',
            null,
            get_string('omh_role_omh_staff', 'local_umass'),
            ROLE_OMH_STAFF,
            $attributes
        );
        $radioarray[] = $mform->createElement(
            'radio',
            'omhsasrole',
            null,
            get_string('omh_role_existing', 'local_umass'),
            ROLE_OMH_EXISTING,
            $attributes
        );
        //$radioarray[] = $mform->createElement(
        //    'radio',
        //    'omhsasrole',
        //    null,
        //    get_string('omh_role_other', 'local_umass'),
        //    ROLE_OMH_OTHER,
        //    $attributes
        //);
        $separator = '<br>';
        $mform->addGroup($radioarray, 'roleomh', 'OMHSAS Role', $separator, false);

        //$mform->addElement(
        //    'text',
        //    'omhsasother',
        //    ' ',
        //    $attributes
        //);

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('cancel', 'cancelbutton', get_string('previous'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('next'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
    public function validation($data, $files) {
        global $DB, $UMASS_COUNTIES;

        //$county = $data['county'];
        //if (!in_array($county, $UMASS_COUNTIES)) {
         //   return array('county' => get_string('countynotexists', 'local_umass'));
        //}
        return array();
    }
}
