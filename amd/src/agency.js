/* globals alert */
/**
 * @module local_umass/agency
 *
 */
define(['jquery', 'jqueryui'], function($) {
    return {
        uuid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        },
        format_results: function(dialogId, resp) {
            var roles = resp.roles;
            var html = '<div id="' + dialogId + '">';

            html += '<h3>' + resp.message + '</h3>';
            html += '<ul>';
            $.each(roles, function(roleid, role) {
                var umassrolefullname = role.fullname;
                html += '<li>' + umassrolefullname + '</li>';
            });

            html += '</ul>';
            html += '<p>' + resp.notice + '</p>';
            html += '</div>';
            return html;
        },
        chooseType: function(selectedType) {
            var radiobox = $('#umass_agency_' + selectedType + '_radio');

            if (!radiobox.prop('checked')) {
                radiobox.prop('checked', true);
            }

            if (this.state.agencytype !== selectedType) {
                this.state.agencytype = selectedType;
                this.state.agencyid = null;
            }

            $.each(this.containers, function(name, elId) {
                if (name !== selectedType) {
                    $('#' + elId).removeClass('selected');
                    // reset input
                    $('#' + elId + ' input.custom-combobox-input').val('');
                }
            });
        },
        initialise: function (roletype, containers, state, buttonid) {
            var _this = this;
            this.containers = containers;
            var aeId = containers.ae;
            var othersId = containers.odp;
            var writeInId = containers.writein;
            this.state = state;
            this.chooseType(state.agencytype);

            $.widget( "custom.combobox", {
                _create: function() {
                    this.wrapper = $( "<span>" )
                    .addClass( "custom-combobox" )
                    .insertAfter( this.element );

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function() {
                    var that = this;
                    var selected = this.element.children( ":selected" ),
                    value = selected.val() ? selected.text() : "";

                    this.input = $( "<input size=48>" )
                    .appendTo( this.wrapper )
                    .val( value )
                    .attr( "title", "" )
                    .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: $.proxy( this, "_source" )
                    })
                    .tooltip({
                        classes: {
                            "ui-tooltip": "ui-state-highlight"
                        }
                    }).focus(function () {
                        $('#' + that.options.elementId + '_radio').trigger('click');
                    });

                    function customItemSelected(event, ui) {
                        event.preventDefault();
                        ui.item.option.selected = true;
                        var state = _this.state;
                        state.agencyid = ui.item.value;

                        $('#' + that.options.elementId).addClass('selected');
                        $('#' + that.options.elementId + '_hidden').val(ui.item.value);
                        this._trigger( "select", event, {
                            item: ui.item.option
                        });
                        that.input.val(ui.item.label);
                    }
                    this._on(this.input, {
                        autocompletefocus: function(event, ui) {
                            customItemSelected.call(this, event, ui);
                        },
                        autocompleteselect: function(event, ui) {
                            customItemSelected.call(this, event, ui);
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function() {
                    var input = this.input,
                    wasOpen = false;

                    $( "<a>" )
                    .attr( "tabIndex", -1 )
                    .attr( "title", "Show All Items" )
                    .tooltip()
                    .appendTo( this.wrapper )
                    .button({
                        icons: {
                            primary: "ui-icon-triangle-1-s"
                        },
                        text: false
                    })
                    .removeClass( "ui-corner-all" )
                    .addClass( "custom-combobox-toggle ui-corner-right" )
                    .on( "mousedown", function() {
                        wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                    })
                    .on( "click", function() {
                        input.trigger( "focus" );

                        // Close if already visible
                        if ( wasOpen ) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete( "search", "" );
                    });
                },

                _source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                    response( this.element.children( "option" ).map(function() {
                        var text = $( this ).text();
                        var value = $( this ).val();
                        if ( this.value && ( !request.term || matcher.test(text) ) ) {
                            return {
                                label: text,
                                value: value,
                                option: this
                            };
                        }
                    }) );
                },

                _removeIfInvalid: function( event, ui ) {

                    // Selected an item, nothing to do
                    if ( ui.item ) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;
                    this.element.children( "option" ).each(function() {
                        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if ( valid ) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                    .val( "" )
                    .attr( "title", value + " didn't match any item" )
                    .tooltip( "open" );
                    this.element.val( "" );
                    this._delay(function() {
                        this.input.tooltip( "close" ).attr( "title", "" );
                    }, 2500 );
                    this.input.autocomplete( "instance" ).term = "";
                },

                _destroy: function() {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            var $body = $("body");
            $body.removeClass("loading");
            $(document).on({
                ajaxStart: function() {
                    $body.addClass("loading");
                },
                ajaxStop: function() {
                    $body.removeClass("loading");
                }
            });

            $('#' + buttonid).click(function() {
                if (_this.state.agencytype === null) {
                    alert('Please choose your agency type.');
                    return;
                }
                if (_this.state.agencytype === 'writein') {
                    var value = $.trim($('#umass_agency_writein_input').val());
                    if (!value) {
                        alert('Writin in field cannot be empty');
                        return;
                    }
                    _this.state.agencyid = value;
                }
                var data = {
                    sesskey: M.cfg.sesskey,
                    action: 'setagency',
                    agencytype: _this.state.agencytype,
                    agencyid: _this.state.agencyid,
                };

                $.ajax({
                    url: M.cfg.wwwroot + '/local/umass/api.php',
                    dataType: "json",
                    type : 'POST',
                    data: data,
                    success: function(data) {
                        if (data.error) {
                            alert(data.error);
                        }
                        if (data.ok) {
                            var dialogId = _this.uuid();
                            var html = _this.format_results(dialogId, data);
                            $("body").append(html);
                            $("#" + dialogId).dialog({
                                dialogClass: "no-close",
                                modal: true,
                                buttons: [
                                    {
                                        text: 'Finish',
                                        click: function() {
                                            window.location.href = M.cfg.wwwroot;
                                            // window.location.href = resp.nexturl;
                                        }
                                }
                                ]
                            });
                        }
                    },
                    error: function(data) {
                        alert(data);
                    }
                });
            });

            $.each(this.containers, function(name, elId) {
                $( "#" + elId + '_radio').click(function() {
                    _this.chooseType(name);
                });
            });

            $.each([aeId, othersId, writeInId], function (index, elId) {
                var element = "#" + elId + '_input';
                $(element).focus(function() {
                    $('#' + elId + '_radio').trigger('click');
                });
            });
            $.each({ae: aeId, odp: othersId}, function (name, elId) {

                $( "#" + elId + "_combobox" ).combobox({'elementId': elId});

                var element = "#" + elId + '_input';
                function itemSelected(event, ui) {
                    var item = ui.item;
                    var state = _this.state;
                    event.preventDefault();

                    state.agencytype = name;
                    state.agencyid = item.value;

                    $('#' + elId).addClass('selected');
                    $('#' + elId + '_input').val(item.label);
                    $('#' + elId + '_hidden').val(item.value);
                }
                $(element).autocomplete({
                    autoFill: true,
                    focus: function(event, ui) {
                        itemSelected(event, ui);
                    },
                    select: function (event, ui) {
                        itemSelected(event, ui);
                    },
                    source: function(request, response) {
                        var term = request.term;
                        $.ajax({
                            dataType: "json",
                            type : 'POST',
                            url: M.cfg.wwwroot + '/local/umass/api.php',
                            data: {
                                sesskey: M.cfg.sesskey,
                                action: 'search',
                                agencytype: name,
                                term: term,
                            },
                            success: function(data) {
                                var agencies = $.map(data, function (o) {
                                    return {label: o.entity, value: o.id};
                                });

                                response(agencies);
                            },
                            error: function(data) {
                                alert(data);
                            }
                        });
                    },
                });
            });
        }
    };
});
