/* globals window */
/* globals alert */
/**
 * @module local_umass/roleselector
 *
 */
define(['jquery', 'jqueryui'], function($) {
    return {
        uuid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        },
        format_results: function(dialogId, resp) {
            var roles = resp.roles;
            var html = '<div id="' + dialogId + '">';

            html += '<h3>' + resp.message + '</h3>';
            html += '0<ul>';
            $.each(roles, function(roleid, value) {
                var umassrole = value.umassrole;
                var umassrolefullname = umassrole.fullname;
                html += '<li>' + umassrolefullname + '1</li>';
            });

            html += '</ul>';
            html += '<p>' + resp.notice + '</p>';
            html += '</div>';
            return html;
        },
        init: function(selectorId, buttonId) {
            var $body = $("body");
            $body.removeClass("loading");
            var self = this;
            function ajax(action, data, success, fail) {
                var u = M.cfg.wwwroot + '/local/umass/api.php?sesskey=' + M.cfg.sesskey + '&action=' + action;
                var httprequest = {
                    url: u,
                    method: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    success: success,
                    error: fail,
                };
                $.ajax(httprequest);
            }
            function foldCat(id) {
                if ($('#' + id).prop('checked')) {
                    var divId = id + '_div';
                    var num = $('#' + divId + ' input:checked').length;
                    if (num === 0) {
                        $('#' + id).trigger('click');
                    }
                }
            }
            $('#' + buttonId).click(function() {
                // clean up first
                var role_cat_ppa = 'idumass_role_ppa';
                var role_cat_idsp = 'idumass_role_idsp';
                var role_cat_aasp_aawssdp = 'idumass_role_aasp_aawssdp';
                var role_cat_others = 'idumass_role_others';
                var role_cat_ae = 'idumass_role_ae';
                $.each([role_cat_ppa, role_cat_idsp, role_cat_aasp_aawssdp, role_cat_others, role_cat_ae], function(index, id) {
                    foldCat(id);
                });

                var role_cat_aasp = 'idumass_role_aasp';
                foldCat(role_cat_aasp);

                var roles = {};
                $("#" + selectorId + " input:checked").each(function() {
                    var rolename = $(this).prop('name');
                    roles[rolename] = {};
                    if ($(this).attr('x-haswritein')) {
                        roles[rolename].description = $('#' + $(this).attr('id') + 'textfield').val();
                    }
                });

                var $body = $("body");

                $(document).on({
                    ajaxStart: function() {
                        $body.addClass("loading");
                    },
                    ajaxStop: function() {
                        $body.removeClass("loading");
                    }
                });

                ajax('registerrole', {'roles': roles}, function(resp) {
                    if (resp.error) {
                        alert(resp.error);
                        window.location.reload(false);
                    } else {
                        var dialogId = self.uuid();
                        var html = self.format_results(dialogId, resp);
                        if (resp.ppaodp) {
                            $("body").append(html);
                            $("#" + dialogId).dialog({
                                dialogClass: "no-close",
                                modal: true,
                                buttons: [
                                    {
                                        text: 'Continue',
                                        click: function() {
                                            if (resp.nexturl) {
                                                window.location.href = resp.nexturl;
                                            }
                                        }
                                }
                                ]
                            });
                        } else {
                            window.location.href = resp.nexturl;
                        }
                    }
                });
            });

            function enableOtherRoles() {
                var  checked = ($('#idumass_role_ppa_aaw').prop('checked')||$('#idumass_role_ppa_pfdsw').prop('checked'));

                var otherRoles = $('.umass_non_ppa_roles input[type=checkbox]');
                if (checked !== false) {
                    $('.umass_non_ppa_roles input[type=checkbox]').each(function() {
                        if($(this).prop('checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
                otherRoles.each(function() {
                    $(this).prop('disabled', checked);
                    if (checked) {
                        $(this).parent().addClass('umass_grey');
                    } else {
                        $(this).parent().removeClass('umass_grey');
                    }
                });

                if (!checked)
                {
                    $('#idumass_role_ae').prop('checked', true);
                    $('#idumass_role_aasp').prop('checked', true);
                    $('#idumass_role_aasp_aawssdp').prop('checked', true);
                    $('#idumass_role_others').prop('checked', true);
                    $('#idumass_role_idsp').prop('checked', true);
                }
                enableWriteIn();
            }

            function enableWriteIn() {
               
                // Prospective Provider Applicant
                if ($('#idumass_role_aasp_others').prop('checked'))
                    $('#idumass_role_aasp_otherstextfield').prop('disabled', false);
                else
                    $('#idumass_role_aasp_otherstextfield').prop('disabled', true);
                
                if ($('#idumass_role_others_other').prop('checked')) 
                    $('#idumass_role_others_othertextfield').prop('disabled', false);
                else
                    $('#idumass_role_others_othertextfield').prop('disabled', true);
                // End - Viet
            }

            function initOtherRoles() {
               
                enableOtherRoles();
                if (!$('#idumass_role_ppa').prop('checked'))
                {
                    if (!$('#idumass_role_others_odp').prop('checked'))
                    {
                        if (!$('#idumass_role_idsp').prop('checked'))
                        {
                            if (!$('#idumass_role_ae').prop('checked'))
                            {
                                $('#idumass_role_ae').prop('checked', true);
                                $('#idumass_role_idsp').prop('checked', true);
                            }
                        }                           

                        $('#idumass_role_aasp').prop('checked', true);
                        $('#idumass_role_others').prop('checked', true);
                        $('#idumass_role_ppa').prop('checked', true);
                    }
                
                    // Viet - children should be disabled when the parent roles are deselected
                    // Administrative Entity
                    //checkboxHandler('ae');
                    // Intellectual Disability Service Provider
                    //checkboxHandler('idsp');
                    // Adult Ausism Service Provider
                    //checkboxHandler('aasp');
                    // sub roles: AAW Specialized Skill Development Provider
                    //checkboxHandler('aasp_aawssdp');
                    // Others
                    //checkboxHandler('others');
                    // Prospective Provider Appicant
                    //checkboxHandler('ppa');    
                }
                else
                {
                    $('#idumass_role_ae').prop('checked', true);
                    $('#idumass_role_idsp').prop('checked', true);
                   
                    $('#idumass_role_aasp').prop('checked', true);
                    $('#idumass_role_aasp_aawssdp').prop('checked', true);
                    $('#idumass_role_others').prop('checked', true);
                }

                enableWriteIn();
                hideParentCheckboxes();
                //alert($("label[for='#idumass_role_ae']").text());
                //style.fontWeight="bold"
            }

            function checkboxClickHandler(cat, e, extrafunc) {
                var checked = $(this).prop('checked');
                var checkboxId = 'idumass_role_' + cat;
                var container = '#idumass_role_' + cat + '_div';
                
                if (checked) {
                    $(this).attr('aria-expanded', true);
                    //$(container + ' ul').attr('aria-hidden', false);//Brad-don't use aria-hidden
                    $(container).show();
                    $(container + ' input[type=checkbox]').each(function() {
                        $(this).prop('disabled', false);
                    });
                    if ($(this).prop('id') === checkboxId) {
                        var subcatId = 'idumass_role_aasp_aawssdp';
                        if (!$('#' + subcatId).prop('checked')) {
                            $('#' + subcatId + '_div ul').show();
                            checkboxHandler('aasp_aawssdp');
                        }
                    }

                } else {
                    $(this).attr('aria-expanded', false);
                    //$(container + ' ul').attr('aria-hidden', true);//Brad-don't use aria-hidden
                    $(container).show();
                    $(container + ' input[type=checkbox]').each(function() {
                        $(this).prop('disabled', true);
                        $(this).prop('checked', false);
                    });

                    //Disable textboxes if needed
                    if (cat == 'aasp')
                        $('#idumass_role_aasp_otherstextfield').prop('disabled', true);

                    if (cat == 'others')
                        $('#idumass_role_others_othertextfield').prop('disabled', true);
                }
                if (typeof(extrafunc) === 'function') {
                    extrafunc.call(this);
                }

            }

             function hideParentCheckboxes() {
                // Administrative Entity
                hideCheckbox('ae');
                // Intellectual Disability Service Provider
                hideCheckbox('idsp');
                // Adult Ausism Service Provider
                hideCheckbox('aasp');
                hideCheckbox('aasp_aawssdp');
                // Others
                hideCheckbox('others');
                // Prospective Provider Appicant
                hideCheckbox('ppa');
            }

            function hideCheckbox(cat){
                var checkboxId = 'idumass_role_' + cat;
                var chkBox = document.getElementById(checkboxId); 
                chkBox.style.display = 'none';
                chkBox.nextSibling.style.fontWeight="bold";
            }

             function checkboxHandler(cat, extrafunc) {
                var checked = $('#idumass_role_' + cat).prop('checked');
                var checkboxId = 'idumass_role_' + cat;
                var container = '#idumass_role_' + cat + '_div';
                if (checked) {
                    $(this).attr('aria-expanded', true);
                    //$(container + ' ul').attr('aria-hidden', false);//Brad-don't use aria-hidden
                    $(container).show();
                    $(container + ' input').each(function() {  //[type=checkbox]
                        $(this).prop('disabled', false);
                    });
                    if ($(this).prop('id') === checkboxId) {
                        var subcatId = 'idumass_role_aasp_aawssdp';
                        if (!$('#' + subcatId).prop('checked')) {
                            $('#' + subcatId + '_div ul').show();
                        }
                    }

                } else {
                    $(this).attr('aria-expanded', false);
                    //$(container + ' ul').attr('aria-hidden', true);//Brad-don't use aria-hidden
                    $(container).show();
                    $(container + ' input[type=checkbox]').each(function() {
                        $(this).prop('disabled', true);
                        $(this).prop('checked', false);
                    });

                    if (cat == 'aasp')
                        $('#idumass_role_aasp_otherstextfield').prop('disabled', true);

                    if (cat == 'others')
                        $('#idumass_role_others_othertextfield').prop('disabled', true);
                }
                if (typeof(extrafunc) === 'function') {
                    extrafunc.call(this, checked);
                }
            }

            // special logic
            var writein_aasp = 'idumass_role_aasp_others';
            var writein_others = 'idumass_role_others_other';
            $('#' + writein_aasp + ', #' + writein_others).click(function() {
                var textFieldId = $(this).attr('id') + 'textfield';
                if (!$(this).prop('checked')) {
                    $('#' + textFieldId).val('');
                }
            });

            function oneCheckboxSelected(shortname) {
                var divId = 'idumass_role_' + shortname + '_div';
                var jqSelector = '#' + divId + '>ul>li>input';
                $(jqSelector).on('change', function() {
                    $(jqSelector).not(this).prop('checked', false);
                    if ($(this).prop('checked')) {
                        if (shortname == 'ae') {
                            if ($('#idumass_role_idsp').prop('checked')) {
                                $('#idumass_role_idsp').trigger('click');
                            }
                        }
                        if (shortname === 'idsp') {
                            if ($('#idumass_role_ae').prop('checked')) {
                                $('#idumass_role_ae').trigger('click');
                            }
                        }
                    }
                });
            }

             function oneCheckbox(shortname) {
                var divId = 'idumass_role_' + shortname + '_div';
                var jqSelector = '#' + divId + '>ul>li>input';
                
                    //$(jqSelector).not(this).prop('checked', false);
                    if ($(jqSelector).prop('checked')) {
                        if (shortname == 'ae') 
                            $('#idumass_role_idsp').prop('checked', false);  
                        else
                            $('#idumass_role_idsp').prop('checked', true);
                        if (shortname === 'idsp') 
                            $('#idumass_role_ae').prop('checked', false);
                        else
                            $('#idumass_role_ae').prop('checked', true);
                    }
                
            }

            // Prospective Provider Applicant
            $('#idumass_role_aasp_others').click(function(e) {
                if ($(this).prop('checked'))
                    $('#idumass_role_aasp_otherstextfield').prop('disabled', false);
                else
                    $('#idumass_role_aasp_otherstextfield').prop('disabled', true);
            });
            $('#idumass_role_others_other').click(function(e) {
                 //if (cat == 'others')
                if ($(this).prop('checked'))
                    $('#idumass_role_others_othertextfield').prop('disabled', false);
                else
                    $('#idumass_role_others_othertextfield').prop('disabled', true);
            });

            // Prospective Provider Applicant
            $('#idumass_role_ppa').click(function(e) {
                checkboxClickHandler.call(this, 'ppa', e, enableOtherRoles);
            });
            $('#idumass_role_ppa_aaw').click(function(e) {
                enableOtherRoles();
            });
            $('#idumass_role_ppa_pfdsw').click(function(e) {
                enableOtherRoles();
            });

            // Administrative Entity
            $('#idumass_role_ae').click(function(e) {
                checkboxClickHandler.call(this, 'ae', e);
            });
            oneCheckboxSelected('ae');
            // Intellectual Disability Service Provider
            $('#idumass_role_idsp').click(function(e) {
                checkboxClickHandler.call(this, 'idsp', e);
            });
            oneCheckboxSelected('idsp');
            // Adult Ausism Service Provider
            $('#idumass_role_aasp').click(function(e) {
                checkboxClickHandler.call(this, 'aasp', e);
            });
            // sub roles: AAW Specialized Skill Development Provider
            $('#idumass_role_aasp_aawssdp').click(function(e) {
                checkboxClickHandler.call(this, 'aasp_aawssdp', e);
            });
            // Others
            $('#idumass_role_others').click(function(e) {
                checkboxClickHandler.call(this, 'others', e);
            });
            // odp staff
            $('#idumass_role_others_odp').click(function () {
                var checked = $(this).prop('checked');
                if (checked !== false) {
                    $('.umass_non_odp_roles input').each(function() {
                        if($(this).prop('checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
            });
            $('#umass_role_selector input').each(function() {
                $(this).click(function() {
                    if($(this).prop('checked')) {
                        var id = $(this).prop('id');
                        if (id != 'idumass_role_others_odp' && id != 'idumass_role_others') {
                            $('#idumass_role_others_odp').prop('checked', false);
                        }
                    }
                });
            });

            function initExistingSelection() {
                // if ppa selected
                initOtherRoles();
            }
            initExistingSelection.call(this);

        }
    };
});
