<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();
define('ROLE_PROFESSIONAL', 1);
define('ROLE_FICA', 2);
define('ROLE_PROFESSIONALANDFICA', 3);
define('ROLE_OMHSAS', 4);

define('ROLE_OMH_IND_NONPRO', 1);
define('ROLE_OMH_IND_PRO', 2);
define('ROLE_OMH_PRO', 3);
define('ROLE_OMH_STAFF', 4);
define('ROLE_OMH_EXISTING', 5);
define('ROLE_OMH_OTHER', 6);

$UMASS_GROUPS = array(
    'G-SC' => 'G-SC',
    'G-SCO' => 'G-SCO',
    'G-ODP' => 'G-ODP',
    'G-PROF' => 'G-PROF',
);
$UMASS_ROLECATEGORIES = array(
    'ppa',
    'idsp',
    'aasp',
    'aasp_aawssdp',
    'ae',
    'others',
);

$UMASS_PPAROLES = array('ppa', 'ppa_aaw', 'ppa_pfdsw');
$UMASS_COUNTIES = array(
    'adams',
    'allegheny',
    'armstrong',
    'beaver',
    'bedford',
    'berks',
    'blair',
    'bradford',
    'bucks',
    'butler',
    'cambria',
    'cameron',
    'carbon',
    'centre',
    'chester',
    'clarion',
    'clearfield',
    'clinton',
    'columbia',
    'crawford',
    'cumberland',
    'dauphin',
    'delaware',
    'elk',
    'erie',
    'fayette',
    'forest',
    'franklin',
    'fulton',
    'greene',
    'huntingdon',
    'indiana',
    'jefferson',
    'juniata',
    'lackawanna',
    'lancaster',
    'lawrence',
    'lebanon',
    'lehigh',
    'luzerne',
    'lycoming',
    'mckean',
    'mercer',
    'mifflin',
    'monroe',
    'montgomery',
    'montour',
    'northampton',
    'northumberland',
    'perry',
    'philadelphia',
    'pike',
    'potter',
    'schuylkill',
    'snyder',
    'somerset',
    'Sullivan',
    'susquehanna',
    'tioga',
    'union',
    'venango',
    'warren',
    'washington',
    'wayne',
    'westmoreland',
    'wyoming',
    'york',
);

/**
 * Add a node in 'My profile settings'
 *
 */
function local_umass_extend_settings_navigation(settings_navigation $settingsnav, context $context) {
    if (isguestuser() || !isloggedin()) {
         return;
    }
    $usernode = $settingsnav->get('usercurrentsettings');
    $icon = null;
    if (!empty($usernode)) {
        // $icon = new pix_icon();
        $usernode->add(
            get_string('register', 'local_umass'),
            new moodle_url('/local/umass/register.php'),
            navigation_node::TYPE_SETTING,
            null,
            'umassregisternode',
            $icon
        );
    }

    // $adminnode = $settingsnav->get('siteadministration');
    // if (!empty($adminnode)) {
        // $adminnode->add(get_string('admin', 'local_umass'),
            // new moodle_url('/local/umass/admin.php'),
            // navigation_node::TYPE_SETTING,
            // null,
            // 'umassadmin',
            // $icon
        // );
    // }
}

function local_umass_extend_navigation(global_navigation $nav) {
    // Add a node in navigation block
    if (isguestuser() || !isloggedin()) {
         return;
    }
    $usernode = $nav->get('myprofile');
    $icon = null;
    if (!empty($usernode)) {
        // $icon = new pix_icon();
        $usernode->add(
            get_string('register', 'local_umass'),
            new moodle_url('/local/umass/register.php'),
            navigation_node::TYPE_SETTING,
            null,
            'umassregisternode',
            $icon
        );
    }
}
