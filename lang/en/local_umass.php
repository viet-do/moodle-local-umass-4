<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'My ODP';
$string['register'] = 'Update MyODP profile';
$string['admin'] = 'UMass Admin';

$string['id'] = 'ID';
$string['role'] = 'Role';
$string['umassrole'] = 'UMass Role';
$string['role_help'] = 'Role';
$string['cohort'] = 'Cohort';
$string['cohort_help'] = 'Cohort';
$string['group'] = 'Group';
$string['group_help'] = 'Group';
$string['courserole'] = 'Moodle course role';
$string['courserole_help'] = 'Moodle course role';
$string['coursename'] = 'Course name';
$string['county'] = 'County';
$string['zip'] = 'ZIP';
$string['addnewagency'] = 'Add new agency';
$string['addnewrole'] = 'Add new role';
$string['rusure'] = 'Are you sure?';

$string['domains'] = 'Email domains';
$string['mailvalidation'] = 'Mail validation';

$string['local_fica'] = 'Individual, Family member, Caregiver, or Advocate (Not a Professional) <br><div class="rolecategoryradio"> * NOTE: Choose this if you hold one of the roles named above and <strong>do not</strong> need to earn professional certificates for completing trainings.</div><br>';
$string['local_professional'] = 'Professional <br><div class="rolecategoryradio"> * NOTE: Choose this if you <strong>are</strong> a professional and you <strong>do</strong> need to earn professional certificates for completing trainings.</div><br>';
$string['local_ficaprofessional'] = 'Individual, Family member, Caregiver, or Advocate AND Professional<br><div class="rolecategoryradio"> * NOTE: Choose this if you are <strong>BOTH</strong> an Individual, Family Member, Caregiver, or Advocate, AND are also a Professional; and you <strong>do</strong> need to earn professional certificates for completing trainings.</div>';
$string['local_omhsas'] = 'Office of Mental Health and Substance Abuse Services (OMHSAS)<br><div class="rolecategoryradio"> * NOTE: Choose this role if you are here for trainings related to OMHSAS.</div>';

$string['countynotexists'] = 'Invalid county name';

$string['prof_role_ppa'] = 'Propective Provider Applicant';
$string['prof_role_idsp'] = 'Intellectual Disablitiy Service Provider';
$string['prof_role_aasp'] = 'Adult Autism Service Provider';
$string['prof_role_ae'] = 'Administrative Entity';
$string['prof_role_bpcs'] = 'BHRS Provider - Children\'s Services';
$string['prof_role_fbata'] = 'FBA Training Attendee or Traniner';
$string['prof_role_bsla'] = 'Act 62 Behavior Specialist License Applicants';
$string['prof_role_mata'] = 'Medication Administration Trainer or Applicant';
$string['prof_role_others'] = 'Other roles';

$string['omh_role_ind_nonpro'] = 'Individual with lived experience, family member, caregiver, or advocate (non-professional roles)';
$string['omh_role_ind_pro'] = 'Individual with lived experience, family member, caregiver, or advocate AND also a professional';
$string['omh_role_pro'] = 'Professional (including service providers, clinicians, local/county staff and administrators, etc.)';
$string['omh_role_omh_staff'] = 'Office of Mental Health and Substance Abuse Services (OMHSAS) staff (including state employees and contractors)';
$string['omh_role_existing'] = '<strong>I have an existing user account with the Office of Developmental Programs (MyODP)</strong>';
$string['omh_role_other'] = 'Other';

$string['domains_help'] = '';
