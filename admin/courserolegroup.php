<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');
$courseid   = required_param('courseid', PARAM_INT);

$id         = optional_param('id', 0, PARAM_INT);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$search     = optional_param('search', '', PARAM_RAW); // Search words. Shortname, fullname, idnumber and summary get searched.
$page       = optional_param('page', 0, PARAM_INT);
$perpage    = optional_param('perpage', null, PARAM_INT);
$blocklist  = optional_param('blocklist', 0, PARAM_INT); // Find courses containing this block.
$modulelist = optional_param('modulelist', '', PARAM_PLUGIN); // Find courses containing the given modules.

$pageurl = new moodle_url('/local/umass/admin/courserolegroup.php', array('courseid' => $courseid));

$PAGE->set_url($pageurl);

require_login();
admin_externalpage_setup('local_umass_coursesearch');
require_capability('moodle/role:assign', context_system::instance());

$editurl   = new moodle_url('/local/umass/admin/courserolegroup.php');
$returnurl = new moodle_url('/local/umass/admin/courserolegroup.php');
$mappinglisturl = new moodle_url('/local/umass/admin/coursemapping.php');

$mform = new \local_umass\form\courserolegroupform($courseid, $pageurl);

$PAGE->navbar->add('Manage umass role and course role/group');

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $mform->get_data()) {
    if (!empty($data->groupid)) {
        $conditions = array('groupid' => $data->groupid, 'courseid' => $courseid, 'umassroleid' => $data->umassroleid);
        if (!$DB->record_exists('local_umass_role_group', $conditions)) {
            $record = new stdclass;
            $record->groupid = $data->groupid;
            $record->courseid = $courseid;
            $record->umassroleid = $data->umassroleid;
            $DB->insert_record('local_umass_role_group', $record);
        }
    }
    if (!empty($data->roleid)) {
        $conditions = array('roleid' => $data->groupid, 'courseid' => $courseid, 'umassroleid' => $data->umassroleid);
        if (!$DB->record_exists('local_umass_role_moodlerole', $conditions)) {
            $record = new stdclass;
            $record->roleid = $data->roleid;
            $record->courseid = $courseid;
            $record->umassroleid = $data->umassroleid;
            $DB->insert_record('local_umass_role_moodlerole', $record);
        }
    }
    redirect($mappinglisturl);
}

echo $OUTPUT->header();
echo $OUTPUT->heading('Add umass role and cohort mapping');

$mform->display();

echo $OUTPUT->footer();
