<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once(dirname(dirname(dirname(__DIR__))) . '/user/profile/lib.php');
require_once(dirname(__DIR__) . '/lib.php');

$agencyid = optional_param('id', 0, PARAM_INT);
$delete = optional_param('delete', 0, PARAM_INT);

if (!empty($delete)) {
    $DB->delete_records('local_umass_agency_odp', array('id' => $delete));
    redirect(new moodle_url('/local/umass/admin/agencyodplist.php'));
}

$params = array(
    'id' => $agencyid,
);
$pageurl = new moodle_url('/local/umass/editagencyodp.php', $params);

$newagency = false;
if ($agencyid === 0) {
    $newagency = true;
}

$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('admin');

require_login();

$PAGE->set_context(context_system::instance());

$heading = 'Manage Agency (Other)';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

$form = new \local_umass\form\editodp();

if (!$newagency) {
    $agency = $DB->get_record('local_umass_agency_odp', array('id' => $agencyid));
    if ($agency) {
        $form->set_data($agency);
    }
}

if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/umass/admin/agencyodplist.php'));
}

echo $OUTPUT->header();

echo $OUTPUT->heading($heading);

if ($formdata = $form->get_data()) {
    if ($newagency) {
        $DB->insert_record('local_umass_agency_odp', $formdata);
        echo $OUTPUT->notification('Saved', 'notifysuccess');
    } else {
        $DB->update_record('local_umass_agency_odp', $formdata);
        echo $OUTPUT->notification('Saved', 'notifysuccess');
    }
    $url = new moodle_url($CFG->wwwroot . '/local/umass/admin/agencyodplist.php');
    echo $OUTPUT->single_button($url, get_string('continue'), 'get');
} else {
    if (!$newagency) {
        $deleteurl = new moodle_url('/local/umass/admin/editagencyodp.php', array('delete' => $agencyid));
        echo $OUTPUT->single_button($deleteurl, get_string('delete'), 'GET');
    }

    $form->display();
}

echo $OUTPUT->footer();
