<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

require_login();
admin_externalpage_setup('local_umass_agencyodplist');
require_capability('moodle/role:assign', context_system::instance());

$heading = 'Agency List (Other)';
$PAGE->navbar->add($heading);

echo $OUTPUT->header();

echo $OUTPUT->heading($heading);

echo $OUTPUT->single_button(new moodle_url('/local/umass/admin/editagencyodp.php',
    array('id' => 0)), 'Add new agency', 'GET');

echo umass_agency_list('odp');

echo $OUTPUT->footer();
