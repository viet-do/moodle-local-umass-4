<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$deleterole = optional_param('deleterole', 0, PARAM_INT);
$deletegrp  = optional_param('deletegrp', 0, PARAM_INT);
$search     = optional_param('search', '', PARAM_RAW); // Search words. Shortname, fullname, idnumber and summary get searched.
$page       = optional_param('page', 0, PARAM_INT);
$perpage    = optional_param('perpage', null, PARAM_INT);
$blocklist  = optional_param('blocklist', 0, PARAM_INT); // Find courses containing this block.
$modulelist = optional_param('modulelist', '', PARAM_PLUGIN); // Find courses containing the given modules.

$issearching = empty($search) && empty($courseid) ? false : true;

require_login();
admin_externalpage_setup('local_umass_coursemapping');
require_capability('moodle/role:assign', context_system::instance());

$editurl   = new moodle_url('/local/umass/admin/coursemapping.php');
$returnurl = new moodle_url('/local/umass/admin/coursemapping.php');

$heading = 'Course role mapping';
$PAGE->navbar->add($heading);

if (!empty($deleterole)) {
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_umass_role_moodlerole', array('id' => $deleterole));
        redirect($returnurl);
    }

    $PAGE->navbar->add(get_string('delete'));

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('rusure', 'local_umass'));

    $editurl->params(array('deleterole' => $deleterole, 'confirm' => 1));

    echo $OUTPUT->confirm(get_string('rusure', 'local_umass'), $editurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}
if (!empty($deletegrp)) {
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_umass_role_group', array('id' => $deletegrp));
        redirect($returnurl);
    }

    $PAGE->navbar->add(get_string('delete'));

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('rusure', 'local_umass'));

    $editurl->params(array('deletegrp' => $deletegrp, 'confirm' => 1));

    echo $OUTPUT->confirm(get_string('rusure', 'local_umass'), $editurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

echo $OUTPUT->header();

echo $OUTPUT->heading($heading);
$sql = "SELECT rmm.id, rmm.umassroleid,r.fullname AS umassrolename,c.fullname AS coursename,
               c.id AS courseid,
               mr.shortname AS roleshortname,mr.name AS rolefullname
          FROM {local_umass_role_moodlerole} rmm
          JOIN {local_umass_role} r ON rmm.umassroleid=r.id
          JOIN {role} mr ON mr.id=rmm.roleid
          JOIN {course} c ON c.id=rmm.courseid";

$roles = $DB->get_records_sql($sql);

$table = new html_table();
$table->attributes['class'] = 'admintable generaltable';
$table->colclasses = array();
$table->head = array ();
$table->head[] = 'Umass role name';
$table->head[] = 'Course name';
$table->head[] = 'Role name';
$table->head[] = '';
$table->id = "roles";

$strdelete = get_string('delete');
foreach ($roles as $role) {
    $buttons = array();
    $deleteurl = new moodle_url('/local/umass/admin/coursemapping.php', array(
        'deleterole' => $role->id,
        'sesskey' => sesskey()));
    $imgattris = array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => $strdelete, 'class' => 'iconsmall');
    $buttons[] = html_writer::link(
        $deleteurl,
        html_writer::empty_tag('img', $imgattrs),
        array('title' => $strdelete)
    );
    $row = array(
        $role->umassrolename,
        $role->coursename,
        $role->rolefullname ? $role->rolefullname : $role->roleshortname,
    );

    $row[] = implode(' ', $buttons);
    $table->data[] = $row;
}

echo html_writer::start_tag('div', array('class' => 'no-overflow'));
echo html_writer::table($table);
echo html_writer::end_tag('div');


$heading = "Course group mappings";
echo $OUTPUT->heading($heading);
$sql = "SELECT gm.id, gm.umassroleid,r.fullname AS umassrolename,c.fullname AS coursename,
               c.id AS courseid,g.name
          FROM {local_umass_role_group} gm
          JOIN {local_umass_role} r ON gm.umassroleid=r.id
          JOIN {groups} g ON g.id=gm.groupid
          JOIN {course} c ON c.id=gm.courseid";

$groups = $DB->get_records_sql($sql);

$grouptable = new html_table();
$grouptable->attributes['class'] = 'admintable generaltable';
$grouptable->colclasses = array();
$grouptable->head = array ();
$grouptable->head[] = 'Umass role name';
$grouptable->head[] = 'Course name';
$grouptable->head[] = 'Group name';
$grouptable->head[] = '';
$grouptable->id = "groups";

$strdelete = get_string('delete');
foreach ($groups as $group) {
    $buttons = array();
    $deleteurl = new moodle_url('/local/umass/admin/coursemapping.php', array(
        'deletegrp' => $group->id,
        'sesskey' => sesskey()));
    $imgattrs = array(
        'src' => $OUTPUT->pix_url('t/delete'), 'alt' => $strdelete, 'class' => 'iconsmall');
    $buttons[] = html_writer::link(
        $deleteurl,
        html_writer::empty_tag('img', $imgattrs),
        array('title' => $strdelete)
    );
    $row = array(
        $group->umassrolename,
        $group->coursename,
        $group->name,
    );

    $row[] = implode(' ', $buttons);
    $grouptable->data[] = $row;
}

echo html_writer::start_tag('div', array('class' => 'no-overflow'));
echo html_writer::table($grouptable);
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
