<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once(dirname(dirname(dirname(__DIR__))) . '/user/profile/lib.php');
require_once(dirname(__DIR__) . '/lib.php');

$roleid = optional_param('id', 0, PARAM_INT);
$params = array(
    'id' => $roleid,
);
$pageurl = new moodle_url('/local/umass/editrole.php', $params);

$newrole = false;
if ($roleid === 0) {
    $newrole = true;
}

$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('admin');

require_login();

$PAGE->set_context(context_system::instance());

$heading = 'Manage roles';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

$form = new \local_umass\form\editrole();
echo $OUTPUT->header();

echo $OUTPUT->heading($heading);

if (!$newrole) {
    $role = $DB->get_record('local_umass_role', array('id' => $roleid));
    if ($role) {
        $form->set_data($role);
    }
}

if ($formdata = $form->get_data()) {
    if ($newrole) {
        $DB->insert_record('local_umass_role', $formdata);
        echo $OUTPUT->notification('Saved', 'notifysuccess');
    } else {
        if (empty($formdata->mailvalidation)) {
            $formdata->mailvalidation = 0;
        }
        $DB->update_record('local_umass_role', $formdata);
        echo $OUTPUT->notification('Saved', 'notifysuccess');
    }
    $url = new moodle_url($CFG->wwwroot . '/local/umass/admin/roles.php');
    echo $OUTPUT->single_button($url, get_string('continue'), 'get');
} else {
    $form->display();
}

echo $OUTPUT->footer();
