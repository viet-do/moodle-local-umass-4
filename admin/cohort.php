<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

$id       = optional_param('id', 0, PARAM_INT);
$delete   = optional_param('delete', 0, PARAM_BOOL);
$confirm  = optional_param('confirm', 0, PARAM_BOOL);

require_login();
admin_externalpage_setup('local_umass_cohort');
require_capability('moodle/role:assign', context_system::instance());


if ($id) {
    $definition = $DB->get_record('local_umass_role_cohort', array('id' => $id), '*', MUST_EXIST);
}

$editurl   = new moodle_url('/local/umass/admin/cohort.php');
$returnurl = new moodle_url('/local/umass/admin/cohort.php');

if ($delete and $definition->id) {
    if ($confirm and confirm_sesskey()) {

        $DB->delete_records('local_umass_role_cohort', array('id' => $definition->id));

        redirect($returnurl);
    }

    $PAGE->navbar->add(get_string('delete'));

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('rusure', 'local_umass'));

    $editurl->params(array('id' => $definition->id, 'delete' => 1, 'confirm' => 1));

    echo $OUTPUT->confirm(get_string('rusure', 'local_umass'), $editurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

$mform = new \local_umass\form\cohortform();

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $mform->get_data()) {
    $record = new stdClass;
    $record->cohortid = $data->cohortid;
    $record->umassroleid = $data->umassroleid;

    if (!$DB->record_exists('local_umass_role_cohort', array(
        'cohortid' => $data->cohortid,
        'umassroleid' => $data->umassroleid,
    ))) {
        $DB->insert_record('local_umass_role_cohort', $record);
    }

    redirect($returnurl);
}

$PAGE->navbar->add('Manage umass role and cohort mappings');

echo $OUTPUT->header();
if ($records = local_umass_cohort_list()) {
    $table = new flexible_table('local_umass_cohort');
    $table->define_columns(array('id', 'cohort', 'cohortdescription', 'role', 'edit'));
    $table->define_headers(array(
        get_string('id', 'local_umass'),
        get_string('cohort', 'local_umass'),
        get_string('description'),
        get_string('umassrole', 'local_umass'),
        get_string('edit')));
    $table->define_baseurl($PAGE->url);
    $table->setup();

    $icon = new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall'));

    foreach ($records as $record) {
        $delete = $OUTPUT->action_icon(
            new moodle_url(
                '/local/umass/admin/cohort.php',
                array('id' => $record->id, 'delete' => 1)
            ),
            $icon
        );

        $table->add_data(array(
            $record->id,
            $record->cohortname,
            $record->description,
            $record->role,
            $delete
        ));
    }

    $table->print_html();
}

echo $OUTPUT->heading('Add umass role and cohort mapping');
$mform->display();

echo $OUTPUT->footer();
