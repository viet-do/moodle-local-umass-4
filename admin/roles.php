<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once(dirname(dirname(dirname(__DIR__))) . '/user/profile/lib.php');
require_once(dirname(__DIR__) . '/lib.php');
require_once($CFG->libdir . '/adminlib.php');

$params = array();
$pageurl = new moodle_url('/local/umass/roles.php', $params);

$deleterole = optional_param('delete', 0, PARAM_INT);
if (!empty($deleterole) and confirm_sesskey()) {
    if ($role = $DB->get_record('local_umass_role', array('id' => $deleterole))) {
        $DB->delete_records('local_umass_role', array('id' => $deleterole));
    }
    redirect($pageurl);
}


admin_externalpage_setup('local_umass_roles');
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('admin');

require_login();

$PAGE->set_context(context_system::instance());

$heading = 'Manage roles';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

echo $OUTPUT->header();

echo $OUTPUT->heading($heading);

$roles = $DB->get_recordset('local_umass_role');

$table = new html_table();
$table->attributes['class'] = 'admintable generaltable';
$table->colclasses = array();
$table->head = array ();
$table->head[] = 'Name';
$table->head[] = 'Require domain validation';
$table->head[] = get_string('edit');
$table->id = "roles";

$stredit   = get_string('edit');
$strdelete = get_string('delete');
$basepath = $CFG->wwwroot . '/local/umass/admin/editrole.php';
foreach ($roles as $role) {
    $buttons = array();
    $editurl = new moodle_url($basepath, array('id' => $role->id));
    $buttons[] = html_writer::link($editurl, html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => $stredit, 'class' => 'iconsmall')), array('title' => $stredit));


    $deleteurl = new moodle_url('/local/umass/roles.php', array('delete' => $role->id, 'sesskey' => sesskey()));
    $buttons[] = html_writer::link($deleteurl, html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => $strdelete, 'class' => 'iconsmall')), array('title' => $strdelete));
    $row = array(
        $role->fullname,
        !empty($role->mailvalidation) ? 'Yes' : 'No',
    );

    $row[] = implode(' ', $buttons);
    $table->data[] = $row;
}
echo html_writer::start_tag('div', array('class' => 'no-overflow'));
echo html_writer::table($table);
echo html_writer::end_tag('div');

// $url = new moodle_url($CFG->wwwroot . '/local/umass/editrole.php', array('id' => 0));
// echo $OUTPUT->single_button($url, get_string('addnewrole', 'local_umass'), 'get');

echo $OUTPUT->footer();
