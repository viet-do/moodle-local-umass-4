<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/cohortrole/locallib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');
require_once($CFG->dirroot . '/local/umass/lib.php');

$id       = optional_param('id', 0, PARAM_INT);
$delete   = optional_param('delete', 0, PARAM_BOOL);
$confirm  = optional_param('confirm', 0, PARAM_BOOL);

require_login();
admin_externalpage_setup('local_umass_group');
require_capability('moodle/role:assign', context_system::instance());


if ($id) {
    $definition = $DB->get_record('local_umass_role_cohort', array('id' => $id), '*', MUST_EXIST);
}

$editurl   = new moodle_url('/local/umass/admin/group.php');
$returnurl = new moodle_url('/local/umass/admin/group.php');

if ($delete and $definition->id) {
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_umass_role_group', array('id' => $definition->id));
        redirect($returnurl);
    }

    $PAGE->navbar->add(get_string('delete'));

    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('rusure', 'local_umass'));

    $editurl->params(array('id' => $definition->id, 'delete' => 1, 'confirm' => 1));

    echo $OUTPUT->confirm(get_string('rusure', 'local_umass'), $editurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

$mform = new \local_umass\form\groupform();

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $mform->get_data()) {
    $record = new stdClass;
    $record->groupname = $data->group;
    $record->groupid   = 0;
    $record->courseid  = 0;
    $record->umassroleid = $data->umassroleid;

    if (!$DB->record_exists('local_umass_role_group', array(
        'groupname' => $data->group,
        'umassroleid' => $data->umassroleid,
    ))) {
        $DB->insert_record('local_umass_role_group', $record);
    }

    redirect($returnurl);
}

$PAGE->navbar->add('Course groups');

echo $OUTPUT->header();

$sql = "SELECT rg.*,r.fullname
          FROM {local_umass_role_group} rg
          JOIN {local_umass_role} r ON r.id=rg.umassroleid";
$records = $DB->get_recordset_sql($sql);
if ($records) {
    $table = new flexible_table('local_umass_group');
    $table->define_columns(array('id', 'groupname', 'role', 'edit'));
    $table->define_headers(array(
        get_string('id', 'local_umass'),
        get_string('group', 'local_umass'),
        get_string('umassrole', 'local_umass'),
        get_string('edit')));
    $table->define_baseurl($PAGE->url);
    $table->setup();

    $icon = new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall'));

    foreach ($records as $record) {
        $delete = $OUTPUT->action_icon(
            new moodle_url(
                '/local/umass/admin/group.php',
                array('id' => $record->id, 'delete' => 1)
            ),
            $icon
        );

        $table->add_data(array(
            $record->id,
            $record->groupname,
            $record->fullname,
            $delete
        ));
    }

    $table->print_html();
}

echo $OUTPUT->heading('Link umass role to course groups');
$mform->display();

echo $OUTPUT->footer();
