<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require(dirname(dirname(dirname(__DIR__))) . '/config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/umass/locallib.php');

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$search     = optional_param('search', '', PARAM_RAW); // Search words. Shortname, fullname, idnumber and summary get searched.
$page       = optional_param('page', 0, PARAM_INT);
$perpage    = optional_param('perpage', null, PARAM_INT);
$blocklist  = optional_param('blocklist', 0, PARAM_INT); // Find courses containing this block.
$modulelist = optional_param('modulelist', '', PARAM_PLUGIN); // Find courses containing the given modules.

$issearching = empty($search) && empty($courseid) ? false : true;

require_login();
admin_externalpage_setup('local_umass_coursesearch');
require_capability('moodle/role:assign', context_system::instance());

$returnurl = new moodle_url('/local/umass/admin/cohort.php');
$mform = new \local_umass\form\coursesearchform(null, null, 'GET');

$heading = 'Search course';
$PAGE->navbar->add($heading);

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $mform->get_data()) {
    if ($issearching) {
        list($courses, $coursescount, $coursestotal) = \core_course\management\helper::search_courses($search, $blocklist, $modulelist, $page, $perpage);
        echo umass_course_search_listing($courses, $coursestotal, null, $page, $perpage, $search);
    }
}
if (!empty($courseid)) {
    $courseform = new \local_umass\form\courserolegroupform($courseid, null, null, 'GET');
    $courseform->display();
} else {
    $mform->display();
}


echo $OUTPUT->footer();
