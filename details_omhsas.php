<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');

require_login(null, false);
$pageurl = new moodle_url(
    '/local/umass/details_omhsas.php');    

$params = array();
$PAGE->requires->css('/local/umass/styles.css');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('incourse');
///////////js/////////////////////////////////////
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->js_call_amd('local_umass/roleselector', 'init');  
////////////////////////////////////////////////
if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}

$form = new \local_umass\form\omhform();  

$heading = 'Choose your OMHSAS role';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

$record = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));

if ($record) {
    $form->set_data($record);
}

$returnurl = new moodle_url('/');
if ($form->is_cancelled()) {
    $prevurl = new moodle_url('/local/umass/register.php');
    redirect($prevurl);
} else if ($formdata = $form->get_data()) {
    $roletype = (int)$formdata->omhsasrole;
    if ($record) {
        if ($roletype != ROLE_OMH_OTHER)
            $omhother = '';
        
        $DB->execute('UPDATE {local_umass_user_data}    
                         SET omhsasrole = ?
                       WHERE userid =?', array($roletype, $USER->id)); 
    }
    //NEINDEX
    omhsas_update_custom_fields($roletype);
    if (($roletype == ROLE_OMH_IND_NONPRO)||($roletype == ROLE_OMH_IND_PRO)||($roletype == ROLE_OMH_OTHER)) {
        $nexturl = new moodle_url(
            '/local/umass/details_omhind.php');

        redirect($nexturl);
    } else if ($roletype == ROLE_OMH_PRO) {
        $nexturl = new moodle_url(
            '/local/umass/details_omhpro.php'); 
       
        redirect($nexturl);
    } else if ($roletype == ROLE_OMH_STAFF) {
        $nexturl = new moodle_url(
            '/local/umass/details_omhstaff.php'); 
        
        redirect($nexturl);
    } else{
        $nexturl = new moodle_url(
            '/local/umass/details_omhexists.php');  
        
        redirect($nexturl);
    }
}

echo $OUTPUT->header();

//echo $OUTPUT->heading('For Individuals, Families, Caregivers and Advocates', 3);
//echo $OUTPUT->heading('Choose the  county you live in', 3);

$note = 'Instructions: Please continue to register for an account if you do not have an account on <a href="/">www.myodp.org</a>. 
If you already have an account, <a href="/login/index.php">please login with those credentials</a>. <br>
If you do not know your username or password, <a href="/login/forgot_password.php">please reset your account</a>. 
<br>DO NOT create more than one account on MyODP.  ';
echo $OUTPUT->notification($note, 'notifymessage');
echo $form->render();
//$form->display();

echo $OUTPUT->footer();
