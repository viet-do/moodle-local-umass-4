<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');
$roletype = required_param('umassmetaroletype', PARAM_INT);

require_login(null, false);
$pageurl = new moodle_url(
    '/local/umass/details_fica.php',
    array('umassmetaroletype' => $roletype)
);

$params = array();
$PAGE->requires->css('/local/umass/styles.css');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('incourse');

if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}

$form = new \local_umass\form\countyform($pageurl);

$heading = 'Choose your county';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

$record = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));

if ($record) {
    $form->set_data($record);
}


$returnurl = new moodle_url('/');
if ($form->is_cancelled()) {
    $prevurl = new moodle_url('/local/umass/register.php');
    redirect($prevurl);
} else if ($formdata = $form->get_data()) {
    if ($record) {
        $record->county = $formdata->county;
        $record->metarole = $roletype;
        $DB->update_record('local_umass_user_data', $record);
    } else {
        $record = new stdclass;
        $record->county = $formdata->county;
        $record->userid = $USER->id;
        $record->metarole = $roletype;
        $DB->insert_record('local_umass_user_data', $record);
    }
    //NEINDEX
    //meta_update_custom_fields($record->metarole);
    if (($roletype & ROLE_PROFESSIONAL) == ROLE_PROFESSIONAL) {
        $nexturl = new moodle_url(
            '/local/umass/details_pro.php',
            array('umassmetaroletype' => $roletype)
        );
        redirect($nexturl);
    } else {
        redirect($returnurl);
    }
}

echo $OUTPUT->header();

echo $OUTPUT->heading('For Individuals, Families, Caregivers and Advocates', 3);
echo $OUTPUT->heading('Choose the  county you live in', 3);

$note = 'NOTE: The Office of Developmental Programs sends critical information by email. 
<br><br>
<i>Please note:</i> If you want critical information to be mailed to you through the postal service, please email your mailing address to: <a href="mailto:RA-PWODP_OUTREACH@pa.gov?subject=request for critical info by postal service">RA-PWODP_OUTREACH@pa.gov</a>   

<br><br>
If clicking the above email address does not open your email program, please copy and paste the email address into your email program.';
echo $OUTPUT->notification($note, 'notifymessage');

$form->display();

echo $OUTPUT->footer();
