<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');

$roletype = required_param('umassmetaroletype', PARAM_INT);

require_login(null, false);
$params = array(
    'umassmetaroletype' => $roletype,
);

$PAGE->requires->css('/local/umass/styles.css');

$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url('/local/umass/agency.php', $params);
$PAGE->set_pagelayout('incourse');
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->add_body_class('loading');

if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}

$aecontainer = 'umass_agency_ae';
$othersinputid = 'umass_agency_others';
$writeininputid = 'umass_agency_writein';
$buttonid = 'umass_process_agency';

$state = new stdclass;
if ($umassuserdata = $DB->get_record('local_umass_user_data', array('userid' => $USER->id))) {
    $agencyid = null;
    if (!empty($umassuserdata->agencytype)) {
        $fieldname = $umassuserdata->agencytype . '_agencyid';
        $agencyid = $umassuserdata->$fieldname;
    }
    $state = array(
        'agencytype' => $umassuserdata->agencytype,
        'agencyid' => $agencyid,
    );
}

$heading = 'Choose the primary agency where you work';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);
$PAGE->requires->js_call_amd('local_umass/agency', 'initialise', array(
    'roletype' => $roletype,
    'containers' => array(
        'ae' => $aecontainer,
        'odp' => $othersinputid,
        'writein' => $writeininputid,
    ),
    'state' => $state,
    $buttonid,
));

echo $OUTPUT->header();
echo $OUTPUT->heading($heading, 3);
$subtitle = 'Select a radio button, start typing your agency name, then select from the list)';
echo $OUTPUT->notification($subtitle, 'notifymessage');

function umass_agency_selector($userdata, $id, $code, $label) {
    global $DB, $USER;
    $size = 48;
    $html = '';

    $radioboxattr = array(
        'type' => 'radio',
        'name' => 'agencytype',
        'id' => $id . '_radio'
    );
    if ($userdata->agencytype === $code) {
        $radioboxattr['checked'] = true;
    }
    $html .= html_writer::empty_tag('input', $radioboxattr);
    $html .= html_writer::tag('label', ' ' . $label . ' ', array('for' => $id . '_radio'));
    $html .= html_writer::empty_tag('br');
    $html .= html_writer::empty_tag('input', array('type' => 'hidden', 'id' => $id . '_hidden'));

    if (in_array($code, array('ae', 'odp'))) {
        $rs = $DB->get_recordset('local_umass_agency_' . $code, null, 'entity');
        $html .= html_writer::start_tag('div', array('class' => "ui-widget"));
        $html .= html_writer::start_tag('select', array('id' => $id . '_combobox'));
        $html .= html_writer::tag('option', null);
        foreach ($rs as $record) {
            $attr = array('value' => $record->id);
            if ($userdata && $userdata->agencytype == $code) {
                $field = $code . '_agencyid';
                if ($userdata->$field == $record->id) {
                    $attr['selected'] = true;
                }
            }
            $html .= html_writer::tag('option', $record->entity, $attr);
        }
        $html .= html_writer::end_tag('select');
        $html .= html_writer::end_tag('div');
    } else {
        $html .= html_writer::empty_tag('input', array(
            'id' => $id . '_input',
            'value' => $userdata->writein_agencyid,
            'type' => 'text',
            'size' => $size,
            'class' => 'custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left ui-autocomplete-input',
        ));
    }

    $html .= html_writer::empty_tag('hr');
    return html_writer::div($html, 'umass_agency_container', array('id' => $id));
}

$html = '';
$html .= umass_agency_selector($umassuserdata, $othersinputid, 'odp', 'Intellectual Disability / Autism');
$html .= umass_agency_selector($umassuserdata, $aecontainer, 'ae', 'Adminstrative Entity');
$html .= umass_agency_selector($umassuserdata, $writeininputid, 'writein', 'All others (write-in)');
echo html_writer::div($html, "amdnotloaded");

echo html_writer::start_tag('div', array('class'=>'umass_nav_buttons_wrapper'));
$url = new moodle_url('/local/umass/details_pro.php', $params);
echo $OUTPUT->single_button($url, get_string('previous'), 'get');

echo '&nbsp';
echo html_writer::tag('button', get_string('submit'), array('id' => $buttonid));
echo html_writer::end_tag('div');


echo html_writer::div(null, 'modal', array('style' => 'overflow: hidden'));

echo $OUTPUT->footer();
