<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $ADMIN->add('root', new admin_category('umass', new lang_string('pluginname', 'local_umass')));
    $str = 'Define roles';
    $ADMIN->add(
        'umass',
        new admin_externalpage(
            'local_umass_roles',
            $str,
            new moodle_url('/local/umass/admin/roles.php'),
            'moodle/role:assign'
        )
    );
    $str = 'AE Agency';
    $ADMIN->add(
        'umass',
        new admin_externalpage(
            'local_umass_agencyaelist',
            $str,
            new moodle_url('/local/umass/admin/agencyaelist.php'),
            'moodle/role:assign'
        )
    );
    $str = 'Other Agency';
    $ADMIN->add(
        'umass',
        new admin_externalpage(
            'local_umass_agencyodplist',
            $str,
            new moodle_url('/local/umass/admin/agencyodplist.php'),
            'moodle/role:assign'
        )
    );
    $str = 'Cohort';
    $ADMIN->add(
        'umass',
        new admin_externalpage(
            'local_umass_cohort',
            $str,
            new moodle_url('/local/umass/admin/cohort.php'),
            'moodle/role:assign'
        )
    );
}
