<?php

//define('CLI_SCRIPT', true);

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');

function cli_umass_import_odp_agencies($filename)
{
    global $DB, $CFG;
    if (($handle = fopen($CFG->dataroot . "/" . $filename, "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->mpi = $data[$c];
                        break;
                    case 1:
                        $record->irsname = $data[$c];
                        break;
                    case 2:
                        $record->entity = $data[$c];
                        break;
                    case 3:
                        $record->odpportion = $data[$c];
                        break;
                    case 4:
                        $record->street = $data[$c];
                        break;
                    case 5:
                        $record->pobox = $data[$c];
                        break;
                    case 6:
                        $record->department = $data[$c];
                        break;
                    case 7:
                        $record->city = $data[$c];
                        break;
                    case 8:
                        $record->state = $data[$c];
                        break;
                    case 9:
                        $record->zip = $data[$c];
                        break;
                }
            }
            if (!$DB->record_exists('local_umass_agency_odp', array('mpi' => $record->mpi))) {
                $DB->insert_record('local_umass_agency_odp', $record);
            }
        }
        fclose($handle);
    }
    else
      echo 'not found';
}

function cli_umass_import_ae_agencies($filename)
{
    global $DB, $CFG;
    if (($handle = fopen($CFG->dataroot . "/" . $filename, "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->region = $data[$c];
                        break;
                    case 1:
                        $record->county = $data[$c];
                        break;
                    case 2:
                        $record->entity = $data[$c];
                        break;
                    case 3:
                        $record->address1 = $data[$c];
                        break;
                    case 4:
                        $record->address2 = $data[$c];
                        break;
                    case 5:
                        $record->city = $data[$c];
                        break;
                    case 6:
                        $record->zip = $data[$c];
                        break;
                    case 7:
                        $record->phone = $data[$c];
                        break;
                    case 8:
                        $record->email = $data[$c];
                        break;
                }
            }
            $conditions = array(
                'email' => $record->email
            );
            if (!$DB->record_exists('local_umass_agency_ae', $conditions)) {
                 $DB->insert_record('local_umass_agency_ae', $record);
            }
        }
        fclose($handle);
    }
    else
      echo 'not found';
}


cli_umass_import_odp_agencies('odp.csv');
cli_umass_import_ae_agencies('ae.csv');