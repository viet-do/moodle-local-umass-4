<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//verifying head

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

function local_umass_get_counties() {
    global $UMASS_COUNTIES;
    $counties = array();
    foreach ($UMASS_COUNTIES as $county) {
        $counties[$county] = ucfirst($county) . ' County';
    }
    return $counties;
}

function umass_get_enrolled_courses($userid) {
    global $USER, $DB;
    $params = array('userid' => $userid);

    $courses = enrol_get_users_courses(
        $params['userid'], true, 'id, shortname, fullname, idnumber, visible,
        summary, summaryformat, format, showgrades, lang, enablecompletion'
    );
    $result = array();

    foreach ($courses as $course) {
        $context = context_course::instance($course->id, IGNORE_MISSING);

        if ($userid != $USER->id and !has_capability('moodle/course:viewparticipants', $context)) {
            // we need capability to view participants
            continue;
        }

        list($enrolledsqlselect, $enrolledparams) = get_enrolled_sql($context);
        $enrolledsql = "SELECT COUNT('x') FROM ($enrolledsqlselect) enrolleduserids";
        $enrolledusercount = $DB->count_records_sql($enrolledsql, $enrolledparams);

        list($course->summary, $course->summaryformat) = external_format_text($course->summary, $course->summaryformat, $context->id, 'course', 'summary', null);

        $result[] = array('id' => $course->id, 'shortname' => $course->shortname, 'fullname' => $course->fullname,
            'idnumber' => $course->idnumber, 'visible' => $course->visible, 'enrolledusercount' => $enrolledusercount,
            'summary' => $course->summary, 'summaryformat' => $course->summaryformat, 'format' => $course->format,
            'showgrades' => $course->showgrades, 'lang' => $course->lang, 'enablecompletion' => $course->enablecompletion
        );
    }

    return $result;
}


function umass_import_roles_from_file() {
    global $DB;
    $data = json_decode(file_get_contents(dirname(__FILE__) . '/initialroles.json'));

    foreach ($data->categories as $category) {
        if (!$DB->record_exists('local_umass_role', array('shortname' => $category->shortname))) {
            $DB->insert_record('local_umass_role', $category);
        }
    }
    umass_import_roles($data, 'ppa');
    umass_import_roles($data, 'aasp');
    umass_import_roles($data, 'aasp_aawssdp');
    umass_import_roles($data, 'idsp');
    umass_import_roles($data, 'ae');
    umass_import_roles($data, 'others');
    umass_import_roles($data, 'roles', true);
}

function umass_import_roles($data, $rolename, $isroot = false) {
    global $DB;
    $parentrole = $DB->get_record('local_umass_role', array('shortname' => $rolename));
    if (!empty($parentrole) or $isroot) {
        $roles = $data->$rolename;
        foreach ($roles as $role) {
            if (!$DB->record_exists('local_umass_role', array('shortname' => $role->shortname))) {
                if (!empty($parentrole)) {
                    $parentid = $parentrole->id;
                    $role->parent = $parentid;
                }
                $DB->insert_record('local_umass_role', $role);
            }
        }
    }
}

function umass_role_category_checkbox($name, $value = null, $checked = false, $label = '', $attributes = array()) {
    $html = '';
    // $html .= html_writer::start_tag('div', array('class'=>'checkbox'));
    // $html .= html_writer::start_tag('label'));

    // checkbox($name, $value, $checked = true, $label = '', array $attributes = null) {
    $html .= html_writer::checkbox($name, $value, $checked, $label, $attributes);
    // $html .= html_writer::end_tag('label'));
    // $html .= html_writer::end_tag('div');
    return $html;
}

function umass_role_tree($catid, $level, $enable=false) {
    global $DB, $USER, $UMASS_ROLECATEGORIES;
    $currentlevel = $level;
    $level++;

    $html = '';
    $sql = 'SELECT r.*
              FROM {local_umass_role} r
             WHERE r.parent = ? ORDER BY r.sortorder';
    $rs = $DB->get_records_sql($sql, array('parent' => $catid));

    $ulclassname = "umass_role_level_${level}";

    $counter = 0;
    if ($counter == 0) {
        $html .= "<ul class='${ulclassname}'>";
    }

    foreach ($rs as $role) {
        $rolename = $role->fullname;
        $shortname = $role->shortname;
        $liclassname = '';

        if (strpos($shortname, 'ppa') === false && !in_array(
            $shortname, array(
            'act62',
            'bhrs',
            'fba'
            )
        )) {
            $liclassname .= ' umass_non_ppa_roles';
        }

        if (!in_array($shortname, array('others_odp', 'others'))) {
            $liclassname .= ' umass_non_odp_roles';
        }
        $checkboxname = 'umass_role_' . $shortname;
        $mv = $role->mailvalidation;

        if (in_array($shortname, array('ppa', 'aasp', 'others', 'aasp_aawssdp'))) {
            $rolename .= ' (select all that apply)';
        }

        if (in_array($shortname, array('ae', 'idsp'))) {
            $rolename .= ' (select one)';
        }

        if (!empty($mv)) {
            $liclassname .= ' umass_mailvalidation';
            $rolename .= ' (email validation required)';
        }

        $attributes = array(
            'id' => "id$checkboxname",
        );
        $conditions = array('userid' => $USER->id, 'umassroleid' => $role->id);

        $checked = false;
        $assignedrole = $DB->get_record('local_umass_user_role', $conditions);
        if ($assignedrole) {
            $checked = true;
        }
        if (!empty($role->haswritein)) {
            $attributes['x-haswritein'] = true;
        }
        $checked = false;
        $style = '';
        $aria = '';
        $liaria= '';
        if (!empty($assignedrole)) {
            $checked = true;
            if (in_array($shortname, $UMASS_ROLECATEGORIES)) {
                $attributes['aria-expanded'] = 'true';
            }
        } else {
            //$style = "display:none"; viet
            //$aria = 'aria-hidden="true"';//Brad-don't use aria-hidden
            if (in_array($shortname, $UMASS_ROLECATEGORIES)) {
                $attributes['aria-expanded'] = 'true'; //FALSE - viet
            }
        }

        $html .= "<li class='${liclassname}' ${liaria}>";
        $html .= html_writer::checkbox($shortname, null, $checked, $rolename, $attributes);
        if (!empty($role->haswritein)) {
            $writeinattrs = array(
                'id' => "id${checkboxname}textfield",
            );
            if (!empty($assignedrole->description)) {
                $writeinattrs['value'] = $assignedrole->description;
            }
            $html .= html_writer::empty_tag('input', $writeinattrs);
        }
        $html .= "<div id='id${checkboxname}_div' style='$style' $aria>";
        $html .= umass_role_tree($role->id, $level, $ckecked);
        $html .= '</div>';
        $html .= '</li>';
        $counter++;
    }
    if ($counter > 0) {
        $html .= '</ul>';
    }

    return $html;
}

function umass_assigned_roles($skip = false) {
    global $USER, $DB, $UMASS_ROLECATEGORIES;
    $existingroles = array();
    $sql = "SELECT r.*,ur.id AS umassuserrole
             FROM {local_umass_role} r
             JOIN {local_umass_user_role} ur
               ON ur.umassroleid=r.id
            WHERE ur.userid=?";
    $rs = $DB->get_recordset_sql($sql, array($USER->id));
    foreach ($rs as $role) {
        if (!$skip || !in_array($role->shortname, $UMASS_ROLECATEGORIES)) {
            $existingroles[$role->id] = $role;
        }
    }
    return $existingroles;
}

function umass_email_validation($role) {
    global $USER;
    // no validation required
    if (empty($role->mailvalidation)) {
        return true;
    }
    $domains = $role->domains;
    if (empty($domains)) {
        return true;
    }
    $email = $USER->email;
    list($emailname, $emaildomain) = explode('@', $email);
    $domains = explode("\n", $domains);
    $ok = false;
    if (count($domains) > 1) {
        foreach ($domains as $domain) {
			$posAt = strpos(strtolower($domain), "@");
			//for domain-only entries (begin with "@"), check against the user's email domain
			if( $posAt == 0 ){
				$pos = strpos(strtolower($domain), strtolower($emaildomain));
				if ($pos !== false) {
					return true;
				}
			//for domain entries in normal email format, check against entire user's email
			} else {
				$pos = strpos(strtolower($domain), strtolower($email));
				if ($pos !== false) {
					return true;
				}
            }
        }
    }

    return $ok;
}

function umass_unset_role_categories($category, $rolenames) {
    if (in_array($category, $rolenames)) {
        // check if category's roles exist
        $subroles = array_filter(
            $rolenames, function ($rolename) use ($category) {
                return strpos($rolename, $category . '_') !== false;
            }
        );
        if (count($subroles) == 0) {
            unset($rolenames[$category]);
        }
    }
    return $rolenames;
}

function umass_get_message_for_metarole($metarole) {
    $message = null;
    if ($metarole == ROLE_PROFESSIONAL) {
        $message = 'Your profile will indicate Professional with the following roles:';
    } else if ($metarole == ROLE_PROFESSIONALANDFICA) {
        $message = 'Your profile will indicate IFCA and a professional with the following roles:';
    } else if ($metarole == ROLE_FICA) {
        $message = 'Your profile will indicate Individual, Family Member, Caregiver, or Advocate';
    }
    return $message;
}

function umass_api_agency_page_message_builder() {
    global $DB, $USER;
    $assignedroles = umass_assigned_roles(true);
    $userdata = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));
    $metarole = $userdata->metarole;
    $message = umass_get_message_for_metarole($metarole);
    $notice = '';

    return array(
        $message,
        $notice,
        $assignedroles,
    );
}

function umass_api_process_user_request($requestroles) {
    global $USER, $DB, $UMASS_ROLECATEGORIES, $UMASS_PPAROLES;
    include_once(__DIR__ . '/lib.php');

    $userdata = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));
    $metarole = $userdata->metarole;
    $message = umass_get_message_for_metarole($metarole);
    $notice = '';
    $nexturl = new moodle_url('/local/umass/agency.php', array('umassmetaroletype' => $metarole));

    $requestroles = (array)$requestroles;

    $rolenames = array_keys($requestroles);
    asort($rolenames);
    if (count($rolenames) < 1) {
        throw new Exception('No roles selected');
    }

    foreach ($UMASS_ROLECATEGORIES as $category) {
        $rolenames = umass_unset_role_categories($category, $rolenames);
    }

    list($sqlfragment, $fragmentparams) = $DB->get_in_or_equal($rolenames, SQL_PARAMS_NAMED);

    $existingroles = umass_assigned_roles();

    $userid = $USER->id;
    $roles = array();
    $sql = "SELECT r.*, ur.description
              FROM {local_umass_role} r
         LEFT JOIN {local_umass_user_role} ur
                ON r.id=ur.umassroleid
             WHERE r.shortname $sqlfragment";

    $rs = $DB->get_recordset_sql($sql, $fragmentparams);
    $ppaselected = false;
    $odpselected = false;
    $ppaonly = false;
    $pparoles = $UMASS_PPAROLES;
    $selectedpparoles = array();
    asort($pparoles);
    foreach ($rs as $role) {
        if (!umass_email_validation($role)) {
            throw new Exception($USER->email . ' is not allowed.');
        }
        $assignedrole = $DB->get_record('local_umass_user_role', array('userid' => $userid, 'umassroleid' => $role->id));

        // adding new role
        if (empty($assignedrole)) {
            // make sure user email is in whitelist
            $record = new stdclass;
            $record->umassroleid = $role->id;
            $record->userid = $USER->id;
            if (!empty($requestroles[$role->shortname]->description)) {
                // description
                $description = $requestroles[$role->shortname]->description;
                $record->description = clean_param($description, PARAM_NOTAGS);
            }
            $DB->insert_record('local_umass_user_role', $record);
        }
        unset($existingroles[(int)$role->id]);
        // don't return role categories
        if (!in_array($role->shortname, $UMASS_ROLECATEGORIES)) {
            // lookup role id and mappings
            $roles[$role->id] = array();
            $roles[$role->id]['umassrole'] = $role;
        }

        if (in_array($role->shortname, $pparoles)) {
            $ppaselected = true;
            umass_process_umassrole_for_user($USER->id, $role);
            $selectedpparoles[] = $role->shortname;
        }
        if ('others_odp' === $role->shortname) {
            $odpselected = true;
            umass_process_umassrole_for_user($USER->id, $role, true);
        }
    }

    // check if only ppa roles selected
    if ($ppaselected) {
        $arraydiff = array_diff($rolenames, $pparoles);
        if (empty($arraydiff)) {
            $ppaonly = true;
            // only ppa role selected
            $nexturl = new moodle_url('/');
        }
        $message = "You’ve selected: ";
        $notice = 'A message will be sent to ODP (Office of Developmental Programs), please wait for further instructions from ODP.';
    }

    if ($odpselected) {
        $nexturl = new moodle_url('/');
    }

    foreach ($existingroles as $index => $roletobedeleted) {
        $DB->delete_records(
            'local_umass_user_role', array(
            'userid' => $USER->id,
            'umassroleid' => $index,
            )
        );
    }
    if ($ppaonly) {
        foreach(array_unique($selectedpparoles) as $pparolename) {
            umass_email_trigger($pparolename);
        }
    }

    return array(
        'ppaodp' => $ppaonly || $odpselected,
        'message' => $message,
        'notice' => $notice,
        'roles' => $roles,
        'nexturl' => $nexturl->out(false),
        'deletedroles' => $existingroles
    );
}

function umass_make_email_user($email, $name = '') {
    $emailuser = new stdClass();
    $emailuser->email = trim(filter_var($email, FILTER_SANITIZE_EMAIL));
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailuser->email = '';
    }
    $name = format_text($name, FORMAT_HTML, array('trusted' => false, 'noclean' => false));
    $emailuser->firstname = trim(filter_var($name, FILTER_SANITIZE_STRING));
    $emailuser->lastname = '';
    $emailuser->maildisplay = true;
    $emailuser->mailformat = 1; // 0 (zero) text-only emails, 1 (one) for HTML emails.
    $emailuser->id = -99;
    $emailuser->firstnamephonetic = '';
    $emailuser->lastnamephonetic = '';
    $emailuser->middlename = '';
    $emailuser->alternatename = '';
    return $emailuser;
}

function umass_email_trigger($rolename, $extra='') {
    global $USER;

    if (!in_array($rolename, array('ppa_aaw', 'ppa_pfdsw'))) {
        return;
    }

    $useremail = $USER->email;
    $subject = '';
    $fromemail = 'support@myodp.org';
    $toemail = '';
    $messagehtml = '';
    $noreplyremail = 'no-reply@myodp.org';
    $messagetext = "User=[$USER->username]  Email=[$USER->email] Selected Professional Role=[$rolename]";

    switch ($rolename) {
        case 'ppa_aaw':
            $toemail = 'ra-pwbasprovenroll@pa.gov';
            $subject = "[MyODP Account Creation Notice] [$rolename] [$useremail]";
        break;
        case 'ppa_pfdsw':
            $toemail = 'ra-pwproviderapp@pa.gov';
            $subject = "[MyODP Account Creation Notice] [$rolename] [$useremail]";
        break;
        case 'others_other':
            $toemail = 'support@myodp.org';
        break;
    }

    $success = email_to_user(
        umass_make_email_user($toemail),
        umass_make_email_user($fromemail),
        $subject,
        $messagetext . $extra,
        $messagehtml,
        '',
        '',
        true,
        $noreplyremail
    );
}
function umass_cohort_add_member_by_name($cohortname, $userid) {
    global $DB, $CFG, $USER;
    include_once($CFG->dirroot . '/cohort/lib.php');
    $cohorts = umass_cohort_get_all_cohorts(0, 100, $cohortname);
    if ($cohorts['totalcohorts'] > 0) {
        $cohort = array_pop($cohorts['cohorts']);
        cohort_add_member($cohort->id, $userid);
    } else {
        throw new Exception($cohortname . ' cohort does not exist');
    }
}

// called by set agency
function umass_api_process_umassroles_for_user() {
    global $DB, $USER;
    $sql = 'SELECT r.*
              FROM {local_umass_role} r
              JOIN {local_umass_user_role} ur ON ur.umassroleid=r.id
             WHERE ur.userid=?';
    $records = $DB->get_records_sql($sql, array($USER->id));
    $results = array();
    foreach ($records as $role) {
        $results[] = umass_process_umassrole_for_user($USER->id, $role, true);
    }
    return $results;
}

function umass_process_umassrole_for_user($userid, $umassrole, $emailtrigger = false) {
    global $DB, $CFG, $USER;
    include_once($CFG->dirroot . '/cohort/lib.php');
    include_once($CFG->dirroot . '/group/lib.php');
    include_once($CFG->dirroot . '/lib/accesslib.php');
    if ($emailtrigger) {
        umass_email_trigger($umassrole->shortname);
    }
    $results = array();

    $roleid = $umassrole->id;
    $userid = $USER->id;
    $cohorts = $DB->get_records('local_umass_role_cohort', array('umassroleid' => $roleid));
    $results['umassrole'] = $umassrole;
    $results['cohorts'] = array();
    foreach ($cohorts as $cohort) {
        cohort_add_member($cohort->cohortid, $userid);
        // $results['cohorts'][$cohort->cohortid] = $DB->get_record('cohort', array('id'=>$cohort->cohortid));
    }
    $groups = $DB->get_records('local_umass_role_group', array('umassroleid' => $roleid));
    $results['rawgroups'] = $groups;
    $results['groups'] = array();
    // foreach ($groups as $group) {
        // assign user to group
        // $group->courseid
        // $group->groupid
        // umass_enrol_user($userid, $group->courseid, 'student');
        // if (groups_add_member($group->groupid, $userid)) {
            // $results['groups'][$group->groupid] = $DB->get_record('groups', array('id'=>$group->groupid));
        // }
    // }
    $moodleroles = $DB->get_records('local_umass_role_moodlerole', array('umassroleid' => $roleid));
    foreach ($moodleroles as $moodlerole) {
        $context = context_course::instance($moodlerole->courseid);
        umass_enrol_user($userid, $moodlerole->courseid, $moodlerole->roleid);
        $id = role_assign($moodlerole->roleid, $userid, $context->id);
        $results['moodleroles'][$id] = true;
    }
    return $results;
}

function local_umass_cohort_list() {
    global $DB;

    $sql = 'SELECT cr.id, c.description, r.fullname AS role, c.id AS cohortid, c.name AS cohortname, r.id AS umassroleid
              FROM {local_umass_role_cohort} cr
              JOIN {cohort} c ON c.id = cr.cohortid
              JOIN {local_umass_role} r ON r.id = cr.umassroleid
          ORDER BY c.name, r.fullname';

    $records = $DB->get_records_sql($sql);

    return $records;
}

function umass_course_search_listing(array $courses, $totalcourses, course_in_list $course = null, $page = 0, $perpage = 20, $search = '') {
    $html = html_writer::start_div('course-listing', array());
    $html .= html_writer::tag('h3', get_string('courses'));
    $html .= html_writer::start_tag('ul', array('class' => 'ml'));
    foreach ($courses as $course) {
        $url = new moodle_url('/local/umass/admin/courserolegroup.php', array('courseid' => $course->id));
        $html .= html_writer::start_tag('li');
        $html .= html_writer::link($url, $course->fullname);
        ;
        $html .= html_writer::end_tag('li');
    }
    $html .= html_writer::end_tag('ul');
    $html .= html_writer::end_div();
    return $html;
}

function umass_enrol_user(
    $userid,
    $courseid,
    $roleidorshortname = null,
    $enrol = 'manual',
    $timestart = 0,
    $timeend = 0,
    $status = null
) {
    global $DB;

    // If role is specified by shortname, convert it into an id.
    if (!is_numeric($roleidorshortname) && is_string($roleidorshortname)) {
        $roleid = $DB->get_field('role', 'id', array('shortname' => $roleidorshortname), MUST_EXIST);
    } else {
        $roleid = $roleidorshortname;
    }

    if (!$plugin = enrol_get_plugin($enrol)) {
        return false;
    }

    $instances = $DB->get_records('enrol', array('courseid' => $courseid, 'enrol' => $enrol));
    if (count($instances) != 1) {
        return false;
    }
    $instance = reset($instances);

    if (is_null($roleid) and $instance->roleid) {
        $roleid = $instance->roleid;
    }

    $plugin->enrol_user($instance, $userid, $roleid, $timestart, $timeend, $status);
    return true;
}

function umass_load_user_profile_data() {
    include_once(dirname(dirname(__DIR__)) . '/user/profile/lib.php');
    $fields = profile_load_data($USER);
}

function umass_agency_list($type) {
    global $DB;
    $rs = $DB->get_recordset('local_umass_agency_' . $type, null, 'entity');
    $html = '';

    $table = new html_table();
    $table->size = array('50%', '50%');
    $row = array();
    foreach ($rs as $record) {
        $link = new moodle_url('/local/umass/admin/editagency'.$type.'.php', array('id' => $record->id));
        $row = array();
        $row[] = html_writer::link($link, $record->entity ? $record->entity : $link->out(false));
        $table->data[] = $row;
    }
    return html_writer::table($table);
}

function umass_cohort_get_all_cohorts($page = 0, $perpage = 25, $search = '') {
    global $DB;

    $fields = "SELECT c.*, ".context_helper::get_preload_record_columns_sql('ctx');
    $countfields = "SELECT COUNT(*)";
    $sql = " FROM {cohort} c
             JOIN {context} ctx ON ctx.id = c.contextid ";
    $params = array();
    $wheresql = '';

    $totalcohorts = $allcohorts = $DB->count_records_sql($countfields . $sql . $wheresql, $params);

    if (!empty($search)) {
        list($searchcondition, $searchparams) = cohort_get_search_query($search, 'c');
        $wheresql .= ($wheresql ? ' AND ' : ' WHERE ') . $searchcondition;
        $params = array_merge($params, $searchparams);
        $totalcohorts = $DB->count_records_sql($countfields . $sql . $wheresql, $params);
    }

    $order = " ORDER BY c.name ASC, c.idnumber ASC";
    $cohorts = $DB->get_records_sql($fields . $sql . $wheresql . $order, $params, $page * $perpage, $perpage);

    // Preload used contexts, they will be used to check view/manage/assign capabilities and display categories names.
    foreach (array_keys($cohorts) as $key) {
        context_helper::preload_from_record($cohorts[$key]);
    }

    return array('totalcohorts' => $totalcohorts, 'cohorts' => $cohorts, 'allcohorts' => $allcohorts);
}

//NEINDEX
function create_custom_fields_for_OMHSAS(){
    create_custom_field("omhsas", "OMHSAS", 1000);
    create_custom_field("omhsas_ind", "OMHSAS non-professional roles", 1010);
    create_custom_field("omhsas_indpro", "OMHSAS Individual AND also a professional", 1020);
    create_custom_field("omhsas_pro", "OMHSAS Professional", 1030);
    create_custom_field("omhsas_staff", "OMHSAS staff", 1040);
    create_custom_field("omhsas_myodp", "OMHSAS have an existing MyODP account", 1050);
}

function create_custom_fields(){
    //create 2 top level roles
    $shortname = "ifca";
    $fullname = "Individual, Family member, Caregiver, Advocate";
    create_custom_field($shortname, $fullname, 10);
        
    $shortname = "prof";
    $fullname = "Professional ";
    create_custom_field($shortname, $fullname, 20);

    umass_import_profile_from_file();
}

function create_custom_fields_from_roles()
{
    global $DB;
    $sql = "SELECT * FROM {local_umass_role}";
    $customfields = $DB->get_records_sql($sql); 
    
    //create 2 top level roles
    $shortname = "ifca";
    $fullname = "Individual, Family member, Caregiver, Advocate";
    create_custom_field($shortname, $fullname, 10);
        
    $shortname = "prof";
    $fullname = "Professional ";
    create_custom_field($shortname, $fullname, 20);

    //sort order
    $order = 2;
    foreach($customfields as $customfield)
    {
        $order = $order + 1;
        $shortname = $customfield->shortname;
        $fullname = $customfield->fullname;
        create_custom_field($shortname, $fullname, $order);
    }
}

function create_custom_field($shortname, $fullname, $order)
{
    global $DB;
   
    $params = array('shortname' => $shortname); //, 'name' => $fullname);
    
    //if custom field doesn't exist
    if (!$DB->record_exists('user_info_field', $params))
    {
        $record = new stdClass();
        $record->shortname  = $shortname;
        $record->name       = $fullname;
        $record->categoryid = 1;
        $record->sortorder  = $order;
        $record->datatype   = 'checkbox';
        $record->defaultdata = 0;

        $lastinsertid = $DB->insert_record('user_info_field', $record, false);
    }
}

function umass_import_profile_from_file() {
    global $DB;
    $data = json_decode(file_get_contents(dirname(__FILE__) . '/profileflds.json'));

    foreach ($data->categories as $category) {
        if (!$DB->record_exists('user_info_field', array('shortname' => $category->shortname))) {
            $record = new stdClass();
            $record->shortname  = $category->shortname;
            $record->name       = $category->fullname;
            $record->categoryid = 1;
            $record->sortorder  = $category->sortorder;
            $record->datatype   = 'checkbox';
            $record->defaultdata = 0;
            $lastinsertid = $DB->insert_record('user_info_field', $record, false);
        }
    }
    umass_import_profiles($data, 'ppa');
    umass_import_profiles($data, 'aasp');
    umass_import_profiles($data, 'aasp_aawssdp');
    umass_import_profiles($data, 'idsp');
    umass_import_profiles($data, 'ae');
    umass_import_profiles($data, 'others');
    umass_import_profiles($data, 'roles');  
}

function umass_import_profiles($data, $rolename) {
    global $DB;
    
    $roles = $data->$rolename;
    foreach ($roles as $role) {
        if (!$DB->record_exists('user_info_field', array('shortname' => $role->shortname))) {
            $record = new stdClass();
            $record->shortname  = $role->shortname;
            $record->name       = $role->fullname;
            $record->categoryid = 1;
            $record->sortorder  = $role->sortorder;
            $record->datatype   = 'checkbox';
            $record->defaultdata = 0;
            $lastinsertid = $DB->insert_record('user_info_field', $record, false);
        }
    }
}

function umass_import_odp_agencies()
{
    global $DB, $CFG;
    if (($handle = fopen(dirname(__FILE__) . '/odp.csv', "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->mpi = $data[$c];
                        break;
                    case 1:
                        $record->irsname = $data[$c];
                        break;
                    case 2:
                        $record->entity = $data[$c];
                        break;
                    case 3:
                        $record->odpportion = $data[$c];
                        break;
                    case 4:
                        $record->street = $data[$c];
                        break;
                    case 5:
                        $record->pobox = $data[$c];
                        break;
                    case 6:
                        $record->department = $data[$c];
                        break;
                    case 7:
                        $record->city = $data[$c];
                        break;
                    case 8:
                        $record->state = $data[$c];
                        break;
                    case 9:
                        $record->zip = $data[$c];
                        break;
                }
            }
            if (!$DB->record_exists('local_umass_agency_odp', array('mpi' => $record->mpi))) {
                $DB->insert_record('local_umass_agency_odp', $record);
            }
        }
        fclose($handle);
    }
}

function umass_additional_odp_agencies()
{
    global $DB, $CFG;
    if (($handle = fopen(dirname(__FILE__) . '/odp21.csv', "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->mpi = $data[$c];
                        break;
                    case 1:
                        $record->entity = $data[$c];
                        break;
                }
            }
            if (!$DB->record_exists('local_umass_agency_odp', array('mpi' => $record->mpi))) {
                $DB->insert_record('local_umass_agency_odp', $record);
            }
        }
        fclose($handle);
    }
}

function umass_additional_odp_agencies22()
{
    global $DB, $CFG;
    if (($handle = fopen(dirname(__FILE__) . '/odp22.csv', "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->mpi = $data[$c];
                        break;
                    case 1:
                        $record->entity = $data[$c];
                        break;
                }
            }
            if (empty($record->mpi)){
                if (!$DB->record_exists('local_umass_agency_odp', array('entity' => $record->entity))) {
                    $DB->insert_record('local_umass_agency_odp', $record);
                }
            }else{
                if (!$DB->record_exists('local_umass_agency_odp', array('mpi' => $record->mpi))) {
                    $DB->insert_record('local_umass_agency_odp', $record);
                }
            }

        }
        fclose($handle);
    }
}

function umass_import_ae_agencies()
{
    global $DB, $CFG;
    if (($handle = fopen(dirname(__FILE__) . '/ae.csv', "r")) !== false) {
        while (($data = fgetcsv($handle, 0, ",")) !== false) {
            $record = new stdClass;
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                switch ($c) {
                    case 0:
                        $record->region = $data[$c];
                        break;
                    case 1:
                        $record->county = $data[$c];
                        break;
                    case 2:
                        $record->entity = $data[$c];
                        break;
                    case 3:
                        $record->address1 = $data[$c];
                        break;
                    case 4:
                        $record->address2 = $data[$c];
                        break;
                    case 5:
                        $record->city = $data[$c];
                        break;
                    case 6:
                        $record->zip = $data[$c];
                        break;
                    case 7:
                        $record->phone = $data[$c];
                        break;
                    case 8:
                        $record->email = $data[$c];
                        break;
                }
            }
            $conditions = array(
                'email' => $record->email
            );
            if (!$DB->record_exists('local_umass_agency_ae', $conditions)) {
                 $DB->insert_record('local_umass_agency_ae', $record);
            }
        }
        fclose($handle);
    }
}

function meta_update_custom_fields($metarole) {
    global $CFG, $DB, $USER;
    require_once $CFG->dirroot . '/user/profile/lib.php';
    require_once $CFG->dirroot . '/user/lib.php';

    switch ($metarole) {
        case ROLE_PROFESSIONALANDFICA:
            $USER->profile_field_ifca = 1;
            $USER->profile_field_prof = 1;
           break;
        case ROLE_PROFESSIONAL:
            $USER->profile_field_ifca = 0;
            $USER->profile_field_prof = 1;
            break;
        case ROLE_OMHSAS:
            $USER->profile_field_omhsas = 1;
            break;
        case ROLE_FICA:
            reset_fica();
            return true;
        default:
            return false;
    }

    try {
        $transaction = $DB->start_delegated_transaction();
        profile_save_data($USER);
        $transaction->allow_commit();
        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
        return false;
    }
}

function omhsas_update_custom_fields($omhsasrole) {
    global $CFG, $DB, $USER;
    require_once $CFG->dirroot . '/user/profile/lib.php';
    require_once $CFG->dirroot . '/user/lib.php';

    switch ($omhsasrole) {
        case ROLE_OMH_IND_NONPRO:
            $USER->profile_field_omhsas_ind = 1;
            $USER->profile_field_omhsas_indpro = 0;
            $USER->profile_field_omhsas_pro = 0;
            $USER->profile_field_omhsas_staff = 0;
            $USER->profile_field_omhsas_myodp = 0;
            break;
        case ROLE_OMH_IND_PRO:
            $USER->profile_field_omhsas_ind = 0;
            $USER->profile_field_omhsas_indpro = 1;
            $USER->profile_field_omhsas_pro = 0;
            $USER->profile_field_omhsas_staff = 0;
            $USER->profile_field_omhsas_myodp = 0;
            break;
        case ROLE_OMH_PRO:
            $USER->profile_field_omhsas_ind = 0;
            $USER->profile_field_omhsas_indpro = 0;
            $USER->profile_field_omhsas_pro = 1;
            $USER->profile_field_omhsas_staff = 0;
            $USER->profile_field_omhsas_myodp = 0;
            break;
        case ROLE_OMH_STAFF:
            $USER->profile_field_omhsas_ind = 0;
            $USER->profile_field_omhsas_indpro = 0;
            $USER->profile_field_omhsas_pro = 0;
            $USER->profile_field_omhsas_staff = 1;
            $USER->profile_field_omhsas_myodp = 0;
            break;
        case ROLE_OMH_EXISTING:
            $USER->profile_field_omhsas_ind = 0;
            $USER->profile_field_omhsas_indpro = 0;
            $USER->profile_field_omhsas_pro = 0;
            $USER->profile_field_omhsas_staff = 0;
            $USER->profile_field_omhsas_myodp = 1;
            break;
        default:
            return false;
    }

    try {
        $transaction = $DB->start_delegated_transaction();
        profile_save_data($USER);
        $transaction->allow_commit();
        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
        return false;
    }
}

function update_custom_fields() {
    global $CFG, $DB, $USER;
    require_once $CFG->dirroot . '/user/profile/lib.php';
    require_once $CFG->dirroot . '/user/lib.php';

    $sql_user_roles = "SELECT shortname FROM {local_umass_role} r
                       WHERE r.id IN 
                      (SELECT umassroleid from {local_umass_user_role} d
                       WHERE d.userid = :userid)";

    $sql_roles = "SELECT shortname from {local_umass_role}";

    $rs_user_roles = $DB->get_records_sql($sql_roles);
    $rs_roles_assigned = $DB->get_records_sql($sql_user_roles, array('userid' => $USER->id));

    foreach ($rs_user_roles as $user_role) {

        //skip ifca and prof custom fields
        if($user_role->shortname == "ifca" || $user_role->shortname == "prof") continue;

        if(in_array($user_role, $rs_roles_assigned)) {
            $USER->{"profile_field_".$user_role->shortname} = 1;
        } else {
            $USER->{"profile_field_".$user_role->shortname} = 0;
        }
    }

    try {
        $transaction = $DB->start_delegated_transaction();
        profile_save_data($USER);
        $transaction->allow_commit();
        profile_load_custom_fields($USER);
        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
        return false;
    }

}

function reset_fica() {
    global $CFG, $DB, $USER;
    require_once $CFG->dirroot . '/user/profile/lib.php';
    require_once $CFG->dirroot . '/user/lib.php';

    $sql_roles = "SELECT shortname from {local_umass_role}";
    $rs_user_roles = $DB->get_records_sql($sql_roles);

    foreach ($rs_user_roles as $user_role) {
        $USER->{"profile_field_".$user_role->shortname} = 0;
    }

    $USER->profile_field_ifca = 1;
    $USER->profile_field_prof = 0;

    try {
        $transaction = $DB->start_delegated_transaction();
        profile_save_data($USER);
        $transaction->allow_commit();
        profile_load_custom_fields($USER);
        return true;

    } catch (Exception $e) {
        $transaction->rollback($e);
        return false;
    }
}

