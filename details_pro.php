<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');

$roletype = required_param('umassmetaroletype', PARAM_INT);
require_login(null, false);

$params = array(
    'umassmetaroletype' => $roletype,
);

$PAGE->requires->css('/local/umass/styles.css');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url('/local/umass/details_pro.php', $params);
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->set_pagelayout('incourse');
$PAGE->add_body_class('loading');

if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}


$selectorid = 'umass_role_selector';
$buttonid = 'umass_process_userrole';
$PAGE->requires->js_call_amd('local_umass/roleselector', 'init', array($selectorid, $buttonid, $roletype));

$heading = 'Choose all your roles.';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);
echo '<p><strong>NOTE: Please read all roles before choosing the role or roles that are most accurate for you.</strong></p>';

echo html_writer::tag('div', umass_role_tree(0, 0), array('id' => $selectorid));

echo html_writer::start_tag('div', array('class'=>'umass_nav_buttons_wrapper'));
$url = new moodle_url('/local/umass/register.php');
echo $OUTPUT->single_button($url, get_string('previous'), 'get');

echo '&nbsp';

echo html_writer::tag('button', get_string('next'), array('id' => $buttonid));
echo html_writer::div(null, 'modal', array('style' => 'overflow: hidden'));
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
