<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');
//$roletype = required_param('umassmetaroletype', PARAM_INT);

require_login(null, false);
$pageurl = new moodle_url(
    '/local/umass/details_omhexists.php');  //,array('umassmetaroletype' => $roletype)

$params = array();
$PAGE->requires->css('/local/umass/styles.css');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('incourse');

if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}

$form = new \local_umass\form\omhexists($pageurl);

$heading = 'You have an existing user';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);


$returnurl = new moodle_url('/');
if ($form->is_cancelled()) {
    $prevurl = new moodle_url('/local/umass/details_omhsas.php');
    redirect($prevurl);
} else if ($formdata = $form->get_data()){
    //NEINDEX
    $returnurl = new moodle_url('/course/index.php?categoryid=529');
    redirect($returnurl);
}

echo $OUTPUT->header();

echo $OUTPUT->heading('You have an existing user account with the Office of Developmental Programs (MyODP)', 3);
echo $OUTPUT->heading('', 3);

$note = 'Notification: Please <a href="../../login/index.php">login in</a> using your MyODP user name and password. If you do not have them please use the <a href="../../login/forgot_password.php">forgot password feature</a>';
echo $OUTPUT->notification($note, 'notifymessage');


$form->display();

echo $OUTPUT->footer();
