all: js

cleanup:
	rm -rf ./amd/build

js: cleanup
	mkdir ./amd/build
	cp ./amd/src/agency.js ./amd/build/agency.min.js
	cp ./amd/src/roleselector.js ./amd/build/roleselector.min.js
cs:
	php ../codechecker/run.php local/umass

deploy: js
	rsync -rv . umass:/var/www/html/moodle/local/umass/
