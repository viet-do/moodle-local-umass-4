<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();


function xmldb_local_umass_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2016073001) {
        // Define table local_umass_role to be created.
        $table = new xmldb_table('local_umass_role');

        // Adding fields to table local_umass_role.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('parent', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('shortname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('fullname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('domains', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table local_umass_role.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_role.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table local_umass_user_data to be created.
        $table = new xmldb_table('local_umass_user_data');

        // Adding fields to table local_umass_user_data.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('umassroleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('primaryagencyid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('county', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        // Adding keys to table local_umass_user_data.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_user_data.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table local_umass_role_cohort to be created.
        $table = new xmldb_table('local_umass_role_cohort');

        // Adding fields to table local_umass_role_cohort.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('umassroleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('cohortid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_umass_role_cohort.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_role_cohort.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table local_umass_role_moodlerole to be created.
        $table = new xmldb_table('local_umass_role_moodlerole');

        // Adding fields to table local_umass_role_moodlerole.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('umassroleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('roleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table local_umass_role_moodlerole.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_role_moodlerole.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table local_umass_role_group to be created.
        $table = new xmldb_table('local_umass_role_group');

        // Adding fields to table local_umass_role_group.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('umassroleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('groupid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_umass_role_group.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_role_group.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2016073001, 'local', 'umass');
    }
    if ($oldversion < 2016073002) {
        // Define field mailvalidation to be added to local_umass_role.
        $table = new xmldb_table('local_umass_role');
        $field = new xmldb_field('mailvalidation', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'type');

        // Conditionally launch add field mailvalidation.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2016073002, 'local', 'umass');
    }
    if ($oldversion < 2016073004) {
        require_once(dirname(__DIR__) . '/locallib.php');
        umass_import_roles_from_file();
        upgrade_plugin_savepoint(true, 2016073004, 'local', 'umass');
    }

    if ($oldversion < 2016082000) {
        // Define table local_umass_user_role to be created.
        $table = new xmldb_table('local_umass_user_role');

        // Adding fields to table local_umass_user_role.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('umassroleid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('description', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        // Adding keys to table local_umass_user_role.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_user_role.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define field umassroleid to be dropped from local_umass_user_data.
        $table = new xmldb_table('local_umass_user_data');
        $field = new xmldb_field('umassroleid');

        // Conditionally launch drop field umassroleid.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016082000, 'local', 'umass');
    }

    if ($oldversion < 2016082001) {
        // Define field groupname to be added to local_umass_role_group.
        $table = new xmldb_table('local_umass_role_group');
        $field = new xmldb_field('groupname', XMLDB_TYPE_CHAR, '255', null, null, null, null, 'groupid');

        // Conditionally launch add field groupname.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016082001, 'local', 'umass');
    }

    if ($oldversion < 2016082002) {
        // Define field metarole to be added to local_umass_user_data.
        $table = new xmldb_table('local_umass_user_data');
        $field = new xmldb_field('metarole', XMLDB_TYPE_INTEGER, '2', null, null, null, null, 'county');

        // Conditionally launch add field metarole.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016082002, 'local', 'umass');
    }
    if ($oldversion < 2016090800) {
        // Define field haswritein to be added to local_umass_role.
        $table = new xmldb_table('local_umass_role');
        $field = new xmldb_field('haswritein', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'domains');

        // Conditionally launch add field haswritein.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field sortorder to be added to local_umass_role.
        $field = new xmldb_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '9999', 'haswritein');

        // Conditionally launch add field sortorder.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016090800, 'local', 'umass');
    }

    if ($oldversion < 2016090803) {
        // Define field umassroleid to be dropped from local_umass_user_data.
        $table = new xmldb_table('local_umass_user_data');
        $field = new xmldb_field('umassroleid');

        // Conditionally launch drop field umassroleid.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Rename field primaryagencyid on table local_umass_user_data to NEWNAMEGOESHERE.
        $field = new xmldb_field('primaryagencyid');

        // Conditionally launch drop field id.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        $field = new xmldb_field('ae_agencyid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'metarole');

        // Conditionally launch add field ae_agencyid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field odp_agencyid to be added to local_umass_user_data.
        $field = new xmldb_field('odp_agencyid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'ae_agencyid');

        // Conditionally launch add field odp_agencyid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field agencytype to be added to local_umass_user_data.
        $field = new xmldb_field('agencytype', XMLDB_TYPE_CHAR, '255', null, null, null, null, 'odp_agencyid');

        // Conditionally launch add field agencytype.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define table local_umass_agency_ae to be created.
        $table = new xmldb_table('local_umass_agency_ae');

        // Adding fields to table local_umass_agency_ae.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('region', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('county', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('entity', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('address1', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('address2', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('city', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('state', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('zip', XMLDB_TYPE_CHAR, '10', null, null, null, null);
        $table->add_field('phone', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        // Adding keys to table local_umass_agency_ae.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_agency_ae.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table local_umass_agency_odp to be created.
        $table = new xmldb_table('local_umass_agency_odp');

        // Adding fields to table local_umass_agency_odp.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('mpi', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('irsname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('entity', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('odpportion', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('street', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('pobox', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('department', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('city', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('state', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('zip', XMLDB_TYPE_CHAR, '255', null, null, null, null);

        // Adding keys to table local_umass_agency_odp.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_umass_agency_odp.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016090803, 'local', 'umass');
    }

    if ($oldversion < 2016090808) {
        // Define field writein_agencyid to be added to local_umass_user_data.
        $table = new xmldb_table('local_umass_user_data');
        $field = new xmldb_field('writein_agencyid', XMLDB_TYPE_CHAR, '255', null, null, null, null, 'odp_agencyid');

        // Conditionally launch add field writein_agencyid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016090808, 'local', 'umass');
    }

    if ($oldversion < 2016090810) {
        require_once(dirname(__DIR__) . '/locallib.php');
        //Create custom profile fields
        create_custom_fields();
        //Import odp agencies();
        //umass_import_odp_agencies();
        //Import ae agencies();
        //umass_import_ae_agencies();

        //delete custom profile fileds prof, ifca
        $sql = "delete from {user_info_data} Where fieldid in 
                (Select uif.id from {user_info_field} uif where shortname in ('prof', 'ifca'))";
        $DB->execute($sql);

        //delete all custom profile created by plugin except prof, ifca
        $sql = "delete from {user_info_data} Where fieldid in 
                (select uif.id  from {local_umass_role} ur inner join {user_info_field} uif on ur.shortname = uif.shortname )";
        $DB->execute($sql);

        //copy all ifca field
        $sql = "Insert INTO {user_info_data}(userid, fieldid, data, dataformat)
                select userid, (select id from {user_info_field} where shortname='ifca') as fieldid, 1, 0 from {local_umass_user_data} where metarole in (2,3)";
        $DB->execute($sql);

        //copy all prof field
        $sql = "Insert INTO {user_info_data}(userid, fieldid, data, dataformat)
                select userid, (select id from {user_info_field} where shortname='prof') as fieldid, 1, 0 from {local_umass_user_data} where metarole in (1,3)";
        $DB->execute($sql);

        //copy all custom profile fileds except prof, ifca
        $sql = "Insert INTO {user_info_data}(userid, fieldid, data, dataformat)
                select uur.userid, uif.id, 1, 0 from {local_umass_user_role} uur inner join {local_umass_role} ur on uur.umassroleid = ur.id 
                inner join {user_info_field} uif on ur.shortname = uif.shortname inner join {local_umass_user_data} ud on uur.userid = ud.userid
                Where ud.umassroleid <> 2";
        $DB->execute($sql);

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2016090810, 'local', 'umass');
    }

	if ($oldversion < 2021032200) {
        // Import additional odp agencies.
		require_once(dirname(__DIR__) . '/locallib.php');
		umass_additional_odp_agencies();

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2021032200, 'local', 'umass');
    }

    if ($oldversion < 2022101000) {
        // Import additional odp agencies 2022.
		require_once(dirname(__DIR__) . '/locallib.php');
		umass_additional_odp_agencies22();

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2022101000, 'local', 'umass');
    }
    if ($oldversion < 2022101400) {
        require_once(dirname(__DIR__) . '/locallib.php');
        
        //create index to increase speed
        $sql = "CREATE INDEX userumass 
                ON {local_umass_user_role} (userid, umassroleid)";
        $DB->execute($sql);
        
        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2022101400, 'local', 'umass');
    }
    if ($oldversion < 2022112000) {
        require_once(dirname(__DIR__) . '/locallib.php');
        
        //create indexes to increase speed
        $sql = "CREATE INDEX datauserid 
                ON {local_umass_user_data} (userid)";
        $DB->execute($sql);

        $sql = "CREATE INDEX cohortumassroleid 
                ON {local_umass_role_cohort} (umassroleid)";
        $DB->execute($sql);

        $sql = "CREATE INDEX cohortcohortid 
                ON {local_umass_role_cohort} (cohortid)";
        $DB->execute($sql);
        
        $sql = "CREATE INDEX groupumassroleid 
                ON {local_umass_role_group} (umassroleid)";
        $DB->execute($sql);

        $sql = "CREATE INDEX moodleroleumassroleid 
                ON {local_umass_role_moodlerole} (umassroleid)";
        $DB->execute($sql);
	
	//Add new fileds for OMHSAS
        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN omhsasrole TINYINT(1)";
        $DB->execute($sql);

        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN membermhpc TINYINT(1)";
        $DB->execute($sql);

        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN serviceregion VARCHAR(255)";
        $DB->execute($sql);

        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN primaryrole VARCHAR(255)";
        $DB->execute($sql);

        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN omhsasbureau VARCHAR(255)";
        $DB->execute($sql);

        $sql = "ALTER TABLE {local_umass_user_data}
		ADD COLUMN servicedeliver VARCHAR(255)";
        $DB->execute($sql);

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2022112000, 'local', 'umass');
    }
    if ($oldversion < 2023011200) {
        require_once(dirname(__DIR__) . '/locallib.php');

        create_custom_fields_for_OMHSAS();

        // Umass savepoint reached.
        upgrade_plugin_savepoint(true, 2023011200, 'local', 'umass');
    }
    return true;
}
