<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

function xmldb_local_umass_install() {
    global $CFG;
    require_once(dirname(__DIR__) . '/locallib.php');
    $result = true;
    umass_import_roles_from_file();
    //Create custom profile fields
    create_custom_fields();
    //Import odp agencies();
    umass_import_odp_agencies();
    //Import ae agencies();
    umass_import_ae_agencies();
    // Import additional odp agencies.
    umass_additional_odp_agencies();
    umass_additional_odp_agencies22();
    return $result;
}




