<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(dirname(__DIR__)) . '/config.php');
require_once(__DIR__ . '/lib.php');
require_once(__DIR__ . '/locallib.php');
require_once("$CFG->libdir/formslib.php");

require_login(null, false);

$params = array();
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_url('/local/umass/register.php', $params);
$PAGE->set_pagelayout('incourse');
$PAGE->requires->css('/local/umass/styles.css');

if (isguestuser()) {
    redirect(new moodle_url('/login/index.php'), get_string('guestsarenotallowed', 'error'), 10);
}

$form = new \local_umass\form\metaroles();

$heading = 'Choose your primary role';
$PAGE->set_title($heading);
$PAGE->set_heading($heading);

$record = $DB->get_record('local_umass_user_data', array('userid' => $USER->id));
$omhrole = $record->omhsasrole;

if ($record) {
    $form->set_data($record);
}

$returnurl = new moodle_url('/');


if ($form->is_cancelled()) {
    redirect($returnurl);
} else if ($formdata = $form->get_data()) {
    $roletype = (int)$formdata->metarole;
    if ($record) {
        $record->metarole = $roletype;
        //$record->omhsasrole = 3;
        $DB->update_record('local_umass_user_data', $record);
    } else {
        $record = new stdclass;
        $record->metarole = $roletype;
        $record->userid = $USER->id;
        $record->umassroleid = 0;
        $DB->insert_record('local_umass_user_data', $record);
    }
    //NEINDEX
    meta_update_custom_fields($record->metarole);
    if (($roletype & ROLE_FICA) == ROLE_FICA) {
        $nexturl = new moodle_url(
            '/local/umass/details_fica.php',
            array('umassmetaroletype' => $roletype)
        );
        // umass_cohort_add_member_by_name('C-IFCA', $USER->id);
        redirect($nexturl);
    } else if ($roletype == ROLE_PROFESSIONAL) {
        $nexturl = new moodle_url(
            '/local/umass/details_pro.php',
            array('umassmetaroletype' => $roletype)
        );
        redirect($nexturl);
    } else if ($roletype == ROLE_OMHSAS) {
        $nexturl = new moodle_url(
            '/local/umass/details_omhsas.php',
            array('umassmetaroletype' => $omhrole)
        );
        redirect($nexturl);
    }
}

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);
if (empty($formdata)) {
    $form->display();
}
echo $OUTPUT->footer();
